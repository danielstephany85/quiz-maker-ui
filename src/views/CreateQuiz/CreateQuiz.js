import React, {useRef, useState} from 'react';
import {useDispatch} from 'react-redux';
import { createQuiz } from '@store/actions/quizActions';
import Form from '@components/formElements/Form'
import FormItem from '@components/formElements/FormItem'
import Box from '@components/elements/Box';
import GridContainer from '@components/elements/GridContainer';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';
import Type from '@components/elements/Type';
import InputWithButton from '@components/formElements/InputWithButton';

const CreateQuiz = props => {
    const dispatch = useDispatch(),
        titleRef = useRef(null),
        [title, setTitle] = useState(''),
        [errors, setErrors] = useState({}),
        [loading, setLoading] = useState(false);

    const handleChange = e => {
        setTitle(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault();
        setErrors({})
        if (title) {
            setLoading(true);
            dispatch(createQuiz({title}))
                .then(quiz => {
                    props.history.push(`/edit-quiz/${quiz.id}`); 
                })
                .catch(() => {
                    setLoading(false);
                });
        }
    }

    return (
        <Box>
            <Type h2 color="inverse" marginTopOff>Create a new Quiz</Type>
            <GridContainer>
                <Grid>
                    <GridItem xs='12'>
                        <Form onSubmit={handleSubmit}>
                            <FormItem xs="12">
                                <InputWithButton 
                                    label="New Quiz Name" 
                                    buttonText="Create Quiz"
                                    loading={loading}
                                    value={title}
                                    onChange={handleChange}
                                    name="title"
                                    error={errors.title}
                                    ref={titleRef}
                                />
                            </FormItem>
                        </Form>
                    </GridItem>
                </Grid>
            </GridContainer>
        </Box>
    );
}

export default CreateQuiz;