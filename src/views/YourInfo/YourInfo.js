import React from 'react';
import { useSelector } from 'react-redux'
import Box from '@components/elements/Box';
import Sheet from '@components/elements/Sheet';
import Type from '@components/elements/Type';
import {getUser} from '@store/selectors/userSelectors';

const YourInfo = props => {
    const { firstName, lastName } = useSelector(getUser);

    return (
        <Box>
            <Type h2 color="inverse" marginTopOff>{`${firstName} ${lastName}`}'s Account Information</Type>
            <Sheet></Sheet>
        </Box>
    );
}

export default YourInfo;