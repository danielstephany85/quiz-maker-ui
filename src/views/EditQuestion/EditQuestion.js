import React, {useState, useEffect} from 'react';
import Box from '@components/elements/Box';
import Type from '@components/elements/Type';
import GridContainer from '@components/elements/GridContainer';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';
import SelectBox from '@components/formElements/SelectBox';
import EditMultipleChoice from './EditMultipleChoice';
import EditTrueOrFalse from './EditTrueOrFalse';
import PopUpMessageContainer from '@components/elements/PopUpMessageContainer';
import PopUpMessage from '@components/elements/PopUpMessage';

import { useDispatch } from 'react-redux'
import { saveQuestion} from '@store/actions/quizActions'

const EditQuestion = ({match, history}) => {
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    const [typeValue, setTypeValue] = useState('0')
    const [popUpMessage, setPopUpMessage] = useState({ type: null, message: '' })
    let orderValue = match.params.order
    let quizId = match.params.quizId

    useEffect(() => {
        const storedTypeValue = localStorage.getItem('editQuestionType');
        if (storedTypeValue) setTypeValue(storedTypeValue)
    }, [typeValue])

    const updateTypeValue = e => {
        const newTypeValue = e.target.value;
        setTypeValue(newTypeValue)
        localStorage.setItem('editQuestionType', newTypeValue)
    }

    useEffect(() => {
        orderValue = match.params.order
        quizId = match.params.quizId

    }, [match.params.quizId, match.params.order]);

    const setQuestionInterface = () => {
        let QuestionInterface;

        switch (typeValue) {
            case '0':
                QuestionInterface = EditMultipleChoice
                break;
            case '1':
                QuestionInterface = EditTrueOrFalse
                break;
            default:
                return null;
        }

        return <QuestionInterface 
                    quizId={quizId}
                    order={orderValue}
                    type={typeValue}
                    backAction={backAction}
                    history={history}
                    saveAndFinish={saveAndFinish}
                    saveAndContinue={saveAndContintue}
                    setPopUpMessage={setPopUpMessage}
                />;
    }

    const backAction = () => {
        history.goBack();
    }

    const saveAndFinish = validateAndGetDataFn => e => {
        e.preventDefault();
        const data = validateAndGetDataFn();
        if (!data) return
        setLoading(true)
        dispatch(saveQuestion(data))
            .then(() => {
                history.push(`/edit-quiz/${quizId}`);
            });
    }

    const saveAndContintue = validateAndGetDataFn => e => {
        e.preventDefault();
        const data = validateAndGetDataFn();
        setLoading(true)
        if (!data) return
        dispatch(saveQuestion(data))
            .then(() => {
                setLoading(false)
                history.push(`/create-question/${quizId}/${parseInt(orderValue) + 1}`);
            });
    }


    return (
        <>
            <Box paddingBottom="0"> 
                <GridContainer>
                    <Grid paddingOff>
                        <GridItem>
                            <Type h2 color="inverse" marginTopOff>Create a new question</Type>
                        </GridItem>
                        <GridItem paddingOff xs='12'>
                                <SelectBox 
                                    label="Select a Question Type"
                                    solid
                                    labelSet
                                    inverse
                                    value={typeValue}
                                    onChange={updateTypeValue}
                                >
                                    <option value="" disabled>Question Type</option>
                                    <option value="0">multiple choice</option>
                                    <option value="1">true or false</option>
                                </SelectBox>
                        </GridItem>
                    </Grid>
                    
                </GridContainer>
            </Box>
            {setQuestionInterface()}
            <PopUpMessageContainer bottom='0' left='0'>
                {popUpMessage.message ? <PopUpMessage messageType="error" handleClose={() => { setPopUpMessage({ type: null, message: '' }) }}>{popUpMessage.message}</PopUpMessage> : null}
            </PopUpMessageContainer>
        </>
    );
}

export default EditQuestion;