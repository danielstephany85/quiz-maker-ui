import React, {useState} from 'react';
import Box from '@components/elements/Box';
import Type from '@components/elements/Type';
import Button from '@components/elements/Button';
import GridContainer from '@components/elements/GridContainer';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';
import TextareaBox from '@components/formElements/TextareaBox';
import SelectBox from '@components/formElements/SelectBox';
import QuestionFooterBox from '@components/elements/QuestionFooterBox';
import QuestionFooter from '@components/elements/QuestionFooter';
import TrueOrFalseCtrl from '@components/interfaces/TrueOrFalseCtrl';

import { requiredMsg } from '@src/constants';

const EditTrueOrFalse = ({ backAction, saveAndContinue, saveAndFinish, setPopUpMessage, quizId, order, type, loading}) => {
    const [values, setValues] = useState({
        questionText: "",
        instructions: "",
        answer: "",
    });
    const [valueErrors, setValueErrors] = useState({});

    const updateValue = (property) => e => {
        const value = e.target.value;
        setValues({ ...values, [property]: value });
    }

    const validateData = () => {
        let valueErrors = {};
        let isValid = true;

        setValueErrors({})

        for(let [key, value] of Object.entries(values)){
            if(!value && key !== 'instructions'){
                valueErrors[key] = requiredMsg;
                isValid = false;
            }
        }

        console.log({ quizId, type, order, ...values });
        if (isValid) {
            return { quizId, type, order, ...values }
        }

        setValueErrors(valueErrors)
        return false;
    }

    return (
        <>
            <Box paddingTop="0" grow overflow='scroll'>
                <GridContainer>
                    <Grid paddingOff>
                        <GridItem paddingOff xs='12'>
                                <TextareaBox
                                    label="Enter Question Text Here"
                                    labelSet
                                    onChange={updateValue('questionText')}
                                    solid
                                    inverse
                                    fullWidth
                                    error={valueErrors.questionText}
                                >
                                </TextareaBox>
                                {/* <TextareaBox
                                    label="Instructions"
                                    labelSet
                                    solid
                                    inverse
                                    fullWidth
                                    minHeight="546x"
                                >
                                </TextareaBox> */}
                        </GridItem>
                    </Grid>
                    <Grid paddingOff>
                        <GridItem xs="12">
                            <Type h3 color="inverse">The true or false options will be presented as follows</Type>
                        </GridItem>
                        <GridItem>
                            <TrueOrFalseCtrl disabled/>
                        </GridItem>
                        <GridItem xs="12" marginTop="12px" paddingOff>
                            <SelectBox
                                label="Select correct option"
                                solid
                                labelSet
                                inverse
                                value={values.answer}
                                onChange={updateValue('answer')}
                                error={valueErrors.answer}
                            >
                                <option value="" disabled>select</option>
                                <option value="true">True</option>
                                <option value="false">False</option>
                            </SelectBox>
                        </GridItem>
                    </Grid>
                </GridContainer>
            </Box>

            <QuestionFooterBox>
                <QuestionFooter>
                    <GridContainer>
                        <Grid>
                            <GridItem paddingOff>
                                <Button primary outlined onClick={backAction}>Back</Button>
                            </GridItem>
                            <GridItem paddingOff grow align="right">
                                <Button primary outlined onClick={saveAndFinish(validateData)}>Save and Finish</Button>
                                <Button primary onClick={saveAndContinue(validateData)} loading={loading}>Save and Continue</Button>
                            </GridItem>
                        </Grid>
                    </GridContainer>
                </QuestionFooter>
            </QuestionFooterBox>
        </>
    );
}

export default EditTrueOrFalse;