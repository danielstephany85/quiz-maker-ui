import React, {useState, useEffect} from 'react';
import Box from '@components/elements/Box';
import Type from '@components/elements/Type';
import Button from '@components/elements/Button';
import GridContainer from '@components/elements/GridContainer';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';
import TextareaBox from '@components/formElements/TextareaBox';
import QuestionOptionEditor from '@components/interfaces/QuestionOptionEditor';
import List from '@components/elements/List';
import QuestionFooterBox from '@components/elements/QuestionFooterBox';
import QuestionFooter from '@components/elements/QuestionFooter';

import {requiredMsg} from '@src/constants';

const EditMultipleChoice = ({ backAction, saveAndContinue, saveAndFinish, quizId, order, type, loading, setPopUpMessage }) => {
    const [options,  setOptions] = useState({length: 0});
    const [optionErrors, setOptionErrors] = useState({});
    const [values, setValues] = useState({
        questionText: "",
        instructions: "",
        answer: "",
    });
    const [errors, setErrors] = useState({});

    const updateValue = (property) => e => {
        const value = e.target.value;
        setValues({ ...values, [property]: value });
    }

    const updateOption = (property) => e => {
        const value = e.target.value;
        setOptions({ ...options, [property]: { ...options[property], optionText: value}});
    }

    const addOption = () => {
        const optionOrder = options.length;
        const valueLabels = "ABCDEF";

        setOptions({ 
            ...options, 
            [optionOrder]: {
                optionText: '', 
                optionValue: valueLabels[optionOrder],
                order: optionOrder
            },
            length: optionOrder + 1
        });
    }

    const markCorrect = (answer) => e => {
        const target = e.target;
        if (target.checked){
            setValues({ ...values, answer: answer });
        }
    }

    const deletOption = (property) => () => {
        if (values.answer.toLowerCase() === options[property].optionValue.toLowerCase()){
            setValues({ ...values, answer: '' });
        }
        const optionsCopy = { ...options}
        delete optionsCopy[property];
        optionsCopy.length--;
        setOptions(optionsCopy);
    }

    const createOptionsArray = () => {
        const optionsArray = [];
        
        Array.from(options).forEach((option, i) => {
            const optionElement = <QuestionOptionEditor
                value={option.optionValue}
                order={option.optionValue}
                key={option.optionValue}
                buttonText={option.optionValue}
                onChange={updateOption(i)}
                markCorrect={markCorrect(option.optionValue)}
                handleDelete={deletOption(i)}
                optionText={option.optionText}
                error={optionErrors[i]}
                name={"option" + i}
                ctrlName={"optionCtrl" + i}
                ></QuestionOptionEditor>;
            optionsArray.push(optionElement)
        });

        return optionsArray;
    }

    const validateData = () => {
        let dataValid = true;
        let answerOptionsErrorObj = {}
        let valuesErrorObj = {}
        let popUpErrorMsg = ''
        let answerOptions = {...options}
        delete answerOptions.length
        setErrors({});
        setOptionErrors({});
        setPopUpMessage({})

        if (typeof quizId === 'undefined' || typeof type === 'undefined' || typeof order === 'undefined') {
            popUpErrorMsg = "data error please got back to your ";
            dataValid = false
        } else if (options.length < 2){
            popUpErrorMsg = "Two or more anwser options are required";
            dataValid = false;
        } else if(!values.answer) {
            popUpErrorMsg = "Mark an option as correct to continue";
            dataValid = false;
        }

        if (!values.questionText) {
            valuesErrorObj['questionTextError'] = requiredMsg
            dataValid = false;
        }

        answerOptions = Object.entries(answerOptions).map(([key, value]) => {
            if (!value.optionText.trim()) {
                answerOptionsErrorObj[key] = requiredMsg
                dataValid = false;
            }
            return value
        })
        
        setPopUpMessage({type: 'error', message: popUpErrorMsg})
        setOptionErrors(answerOptionsErrorObj)
        setErrors(valuesErrorObj)

        console.log({
            quizId,
            type,
            order,
            ...values,
            answerOptions
        });
        if (dataValid) {
            return {quizId, type, order, ...values, answerOptions}
        }

        return false;
    }

    return (
        <>
            <Box paddingTop="0" grow overflow='scroll'>
                <GridContainer>
                    <Grid paddingOff>
                        <GridItem paddingOff xs='12'>
                                <TextareaBox
                                    label="Enter Question Text Here"
                                    labelSet
                                    value={values.questionText}
                                    onChange={updateValue('questionText')}
                                    solid
                                    inverse
                                    fullWidth 
                                    error={errors.questionTextError}
                                    >
                                </TextareaBox>
                                {/* <TextareaBox
                                    label="Instructions"
                                    labelSet
                                    value={values.instructions}
                                    onChange={updateValue('instructions')}
                                    solid
                                    inverse
                                    fullWidth
                                    minHeight="546x"
                                >
                                </TextareaBox> */}
                        </GridItem>
                    </Grid>
                    <Grid paddingOff>
                        <GridItem xs="12">
                            <Type h3 color="inverse">Create answer options below</Type>
                        </GridItem>
                        <GridItem xs="12">
                            <List>
                                {createOptionsArray()}
                            </List>
                        </GridItem>
                        <GridItem xs="12" marginTop="12px">
                            {options.length < 6 ? <Button primary onClick={addOption}>Add Answer Option</Button> : null }
                        </GridItem>
                    </Grid>
                </GridContainer>
            </Box>

            <QuestionFooterBox>
                <QuestionFooter>
                    <GridContainer>
                        <Grid>
                            <GridItem paddingOff>
                                <Button primary outlined onClick={backAction}>Back</Button>
                            </GridItem>
                            <GridItem paddingOff grow align="right">
                                <Button primary outlined onClick={saveAndFinish(validateData)}>Save and Finish</Button>
                                <Button primary onClick={saveAndContinue(validateData)} loading={loading}>Save and Continue</Button>
                            </GridItem>
                        </Grid>
                    </GridContainer>
                </QuestionFooter>
            </QuestionFooterBox>
        </>
    );
}

export default EditMultipleChoice;