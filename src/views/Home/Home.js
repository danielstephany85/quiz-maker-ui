import React from 'react';
import styled from 'styled-components';
import Box from "@components/elements/Box";
import Type from "@components/elements/Type";
import Grid from "@components/elements/Grid";
import GridItem from "@components/elements/GridItem";
import Button from "@components/elements/Button";
import { Link } from  "react-router-dom";

const Title = styled.h1`
    font-family: 'Pacifico', cursive;
    color: ${props => props.theme.bgSecondary};
    font-size: 38px;
    letter-spacing: 2px;
`;

const Home = props => {

    return (
        <React.Fragment>
            <Box marginTop="20vh" align="center">
                <Title>Quiz Maker</Title>
                <Type h2 color="inverse" >Easily create and distribute custom quizes </Type>
                <Grid paddingOff>
                    <GridItem marginTop="20px" xs="12" align="center">
                        <Button secondary outlined component={Link} to="/sign-up">Sign Up</Button>
                        <Button primary component={Link} to="/sign-in">Sign In</Button>
                    </GridItem>
                </Grid>
            </Box>
        </React.Fragment>
    );
}

export default Home;