import React, { useState, useRef} from 'react';
import Grid from "@components/elements/Grid";
import GridItem from "@components/elements/GridItem";
import Sheet from '@components/elements/Sheet';
import Type from '@components/elements/Type';
import Form from '@components/formElements/Form';
import FormItem from '@components/formElements/FormItem';
import TextBox from '@components/formElements/TextBox';
import Button from '@components/elements/Button';
import MainHeader from '@components/interfaces/MainHeader';
import { hasValidInputs } from '@utils'
import { logInUser } from '@store/actions/userActions';

import { useDispatch } from 'react-redux'

export default function SignIn(props){
    const dispatch = useDispatch(),
        elementRefs = {
            email: useRef(null),
            password: useRef(null)
        },
        [values, setValues] = useState({
            email: "",
            password: "",
        }),
        [errors, setErrors] = useState({}),
        [loading, setLoading] = useState(false);


    const updateValue = name => e => {
        setValues({...values, [name]: e.target.value})
    }

    const handleSubmit = e => {
        e.preventDefault();
        setErrors({})
        if (hasValidInputs(values, elementRefs, setErrors)){
            setLoading(true);
            dispatch(logInUser(values))
                .then( res => {props.history.push("/your-quizzes");});
            
        }
    }

    return(
        <React.Fragment>
            <MainHeader />
            <Grid>
                <GridItem xs="12">
                    <Sheet maxWidth="500px" marginTop="12vh">
                        <Type h1 marginTopOff color="primary" >Sign In</Type>
                        <Form offsetPadding onSubmit={handleSubmit} noValidate={true}>
                            <FormItem xs="12">
                                <TextBox fullWidth name="email" label="Email" type="email" value={values.email} onChange={updateValue("email")} required error={errors.email} ref={elementRefs.email}/>
                            </FormItem>
                            <FormItem xs="12">
                                <TextBox fullWidth name="password" label="Password" type="password" value={values.password} onChange={updateValue("password")} required error={errors.password} ref={elementRefs.password}/>
                            </FormItem>
                            <GridItem xs="12" align={"right"} marginTop={"16px"}>
                                <Button primary type="submit" loading={loading} >Submit</Button>
                            </GridItem>
                        </Form>
                    </Sheet>
                </GridItem>
            </Grid>
        </React.Fragment>
    );
}
