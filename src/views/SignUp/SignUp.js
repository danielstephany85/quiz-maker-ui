import React, {useState, useRef} from 'react';
import Grid from "@components/elements/Grid";
import GridItem from "@components/elements/GridItem";
import Sheet from '@components/elements/Sheet';
import Type from '@components/elements/Type';
import Form from '@components/formElements/Form';
import FormItem from '@components/formElements/FormItem';
import TextBox from '@components/formElements/TextBox';
import Button from '@components/elements/Button';
import MainHeader from '@components/interfaces/MainHeader';
import { hasValidInputs } from '@utils'

// import { useDispatch } from 'react-redux'

export default function SignUp(){
    // const dispatch = useDispatch()
    const elementRefs = {
        firstName: useRef(null),
        lastName: useRef(null),
        email: useRef(null),
        password: useRef(null),
        password2: useRef(null)
    }
    const [values, setValues] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        password2: ""
    }),
    [errors, setErrors] = useState({});

    const updateValue = name => e => {
        setValues({...values, [name]: e.target.value})
    }

    const handleSubmit = e => {
        e.preventDefault();
        setErrors({});
        
        if(hasValidInputs(values, elementRefs, setErrors)){
            console.log(values);
        }
        console.log(errors);
    }

    return(
        <React.Fragment>
            <MainHeader></MainHeader>
            <Grid>
                <GridItem xs="12">
                    <Sheet maxWidth="500px" marginTop="12vh">
                        <Type h1 marginTopOff color="primary" >Sign Up</Type>
                        <Form offsetPadding onSubmit={handleSubmit} noValidate={true}>
                            <FormItem xs="12">
                                <TextBox fullWidth name="firstName" label="First Name" onChange={updateValue("firstName")} error={errors.firstName} ref={elementRefs.firstName} required/>
                            </FormItem>
                            <FormItem xs="12">
                                <TextBox fullWidth name="lastName" label="Last Name" onChange={updateValue("lastName")} error={errors.lastName} ref={elementRefs.lastName} required/>
                            </FormItem>
                            <FormItem xs="12">
                                <TextBox fullWidth name="email" type="email" label="Email Name" onChange={updateValue("email")} error={errors.email} ref={elementRefs.email} required/>
                            </FormItem>
                            <FormItem xs="12">
                                <TextBox fullWidth name="password" type="password" label="Password" onChange={updateValue("password")} error={errors.password} ref={elementRefs.password} required/>
                            </FormItem>
                            <FormItem xs="12">
                                <TextBox fullWidth name="password2" type="password" label="Re-Enter Password" onChange={updateValue("password2")} error={errors.password2} ref={elementRefs.password2} required/>
                            </FormItem>
                            <GridItem xs="12" align={"right"} marginTop={"16px"}>
                                <Button primary type="submit">Submit</Button>
                            </GridItem>
                        </Form>
                    </Sheet>
                </GridItem>
            </Grid>
        </React.Fragment>
    );
}
