import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import { logoutUser } from '@store/actions/userActions';
import {Link} from 'react-router-dom';
import Box from '@components/elements/Box';
import Sheet from '@components/elements/Sheet';
import Type from '@components/elements/Type';
import Hr from '@components/elements/Hr';
import Button from '@components/elements/Button';
import List from '@components/elements/List';
import ListItem from '@components/elements/ListItem';
import TextLink from '@components/elements/TextLink';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';

const Settings = props => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    const handleLogout = () => {
        if(!loading){
            setLoading(true);
            dispatch(logoutUser());
        }
    }

    return (
        <Box>
            <Type h2 color="inverse" marginTopOff>Settings</Type>
            <Sheet>
                <List>
                    <ListItem>
                        <TextLink component={Link} to="your-info">View and edit your info</TextLink>
                    </ListItem>
                </List>

                <Hr />
                <Grid>
                    <GridItem paddingOff marginTop="20px" xs="12">
                        <Button secondary outlined onClick={handleLogout}>logout</Button>
                    </GridItem>
                </Grid>
            </Sheet>
        </Box>
    )
}

export default Settings;