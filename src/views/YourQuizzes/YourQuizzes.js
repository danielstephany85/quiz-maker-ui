import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {getQuizzes} from '@store/actions/quizActions';
import GridContainer from '@components/elements/GridContainer'
import Grid from '@components/elements/Grid'
import GridItem from '@components/elements/GridItem'
import Box from '@components/elements/Box';
import Type from '@components/elements/Type';
import BaseInput from '@components/formElements/BaseInput';
import List from '@components/elements/List';
import ListItem from '@components/elements/ListItem';
import SelectorBlock from '@components/interfaces/SelectorBlock';

import NoQuizzesMessageBox from './NoQuizzesMessageBox';

import {getUser} from '@store/selectors/userSelectors';

const YourQuizzes = props => {
    const dispatch = useDispatch();
    const { firstName, lastName } = useSelector(getUser);
    const [quizzes, setQuizzes] = useState([]);
    const [pageLoading, setPageLoading] = useState(true)

    useEffect(() => {
        dispatch(getQuizzes())
        .then(json => {
            if( json.data && json.data.quizzes){
                setQuizzes(json.data.quizzes);
            }
            setPageLoading(false);
        });
    }, [dispatch])

    if (!pageLoading && !quizzes.length){
        return <NoQuizzesMessageBox />
    }

    const quizListItems = quizzes.map(quiz => <ListItem key={quiz.id} data-id={quiz.id}><SelectorBlock quiz={quiz}>{quiz.title}</SelectorBlock></ListItem>);

    return (
        <Box>
            <GridContainer>
                <Grid>
                    <GridItem paddingOff xs="12">
                        <Type h2 color="inverse" marginTopOff>{`${firstName} ${lastName}`}'s Quizzes</Type>
                    </GridItem>
                    <GridItem paddingOff xs="6">
                        <BaseInput paddingOff inverse fullWidth maxWidth="340px" label="Search for a quiz" iconClass="fas fa-search"/>
                    </GridItem>
                    <GridItem paddingOff marginTop="20px" xs="12">
                        <Type h3 color="inverse" >Your Quizzes</Type>
                        <List>
                            {quizListItems}
                        </List>
                    </GridItem>
                </Grid>
            </GridContainer>
        </Box>
    );
}

export default YourQuizzes;