import React from 'react';
import {Link} from 'react-router-dom';
import Button from '@components/elements/Button';
import MessageBox from '@components/interfaces/MessageBox';

export default function NoQuizzesMessageBox(props){
    return (
       <MessageBox 
            title="Oh noes..."
            subtitle="Look like there are no quizzes yet."
            body="Go to Create Quiz to make your first quiz."
            button={<Button primary component={Link} to="create-quiz">Create Quiz</Button>}
       />
    )
}