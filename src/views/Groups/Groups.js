import React from 'react';
import Box from '@components/elements/Box';
import Sheet from '@components/elements/Sheet';
import Type from '@components/elements/Type';


const Groups = props => {

    return (
        <Box>
            <Type h2 color="inverse" marginTopOff>Your Groups</Type>
            <Sheet>test</Sheet>
        </Box>
    );
}

export default Groups;