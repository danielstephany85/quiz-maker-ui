import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getQuiz} from '@store/selectors/quizSelectors';
import {updateQuiz} from '@store/actions/quizActions';

import Modal from '@components/interfaces/Modal';
import Form from '@components/formElements/Form';
import FormItem from '@components/formElements/FormItem';
import TextBox from '@components/formElements/TextBox';
import TextareaBox from '@components/formElements/TextareaBox';
import Button from '@components/elements/Button';
import Box from '@components/elements/Box';
import ModalHeader from '@components/elements/ModalHeader';

const QuizEditModal = ({ modalOpen, handleClose, showSuccess, showError}) => {
    const dispatch = useDispatch(),
        quiz = useSelector(getQuiz),
        [values, setValues] = useState({}),
        [titleError, setTitleError] = useState(''),
        [loading, setLoading] = useState(false);

        useEffect(() => {
            setValues({
                title: quiz.title || "",
                description: quiz.description || ""
            })
        },[quiz.title, quiz.description]);

        const updateValue = e => {
            setValues({...values, [e.target.name]: e.target.value})
        }

        const handleSubmit = e => {
            e.preventDefault();
            
            if (loading) return;

            if(!values.title){
                setTitleError('a valid title is required');
                return;
            }
            
            setLoading(true);
            dispatch(updateQuiz(quiz.id, values))
                .then( () => {
                    setLoading(false);
                    showSuccess(true);
                    handleClose();
                })
                .catch(() => {
                    setLoading(false);
                    showError(true);
                });
        }

    return (
        <Modal 
            modalOpen={modalOpen}
            handleClose={handleClose}
        >
            <React.Fragment>
                <ModalHeader action={handleClose}>Update Quiz Title and Description</ModalHeader>
                <Box>
                    <Form offsetPadding onSubmit={handleSubmit}>
                        <FormItem xs='12'>
                            <TextBox 
                                label="Title"
                                value={values.title || ''}
                                onChange={updateValue}
                                name="title"
                                fullWidth
                                error={titleError}
                            />
                        </FormItem>
                        <FormItem xs='12'>
                            <TextareaBox 
                                label="Description"
                                value={values.description || ''}
                                onChange={updateValue}
                                name="description"
                                fullWidth
                                minHeight="200px"
                            />
                        </FormItem>
                        <FormItem paddingOff={false} xs="12" align="right">
                            <Button secondary outlined onClick={handleClose}>Cancel</Button>
                            <Button primary type="submit" loading={loading} >Update</Button>
                        </FormItem>
                    </Form>
                </Box>
            </React.Fragment>
        </Modal>
    );
}

export default QuizEditModal;