import React from 'react';
import styled from 'styled-components';
import InfoBlock from '@components/interfaces/InfoBlock';

const editQuestionList = ({className, questions}) => {
    const questionTypes = {
        "0": "multiple choice",
        "1": "true or false"
    }

    const createQuestionBlocks = questionsArray => questionsArray.map(({ id, type, questionText }) => <InfoBlock key={id} title={questionTypes[type]} body={questionText} />)

    return (
        <div className={className}>
            <div className="header">
                <div className="header__left"></div>
                <div className="header__right"></div>
            </div>
            <div className="number-section">

            </div>
            <div className="quiz-item-section">
                {createQuestionBlocks(questions)}
            </div>
        </div>
    )
}

const EditQuestionList = styled(editQuestionList)`
    display: flex;
    flex-wrap: wrap;
    .header {
        width: 100%;
    }
    .number-section {
        width: 200px;
    }
    .quiz-item-section {
        flex-grow: 1;
    }
    ${InfoBlock} {
        margin-bottom: ${props => props.theme.padding}px;
        height: 80px;
    }
`;

export default EditQuestionList;