import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux'
import { getQuiz } from '@store/actions/quizActions';
import { getQuiz as getQuizSelecor } from '@store/selectors/quizSelectors';
import Box from '@components/elements/Box';
import GridContainer from '@components/elements/GridContainer';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';
import Type from '@components/elements/Type';
import InfoBlock from '@components/interfaces/InfoBlock';
import Button from '@components/elements/Button';
import PopUpMessageContainer from '@components/elements/PopUpMessageContainer';
import PopUpMessage from '@components/elements/PopUpMessage';
import QuizEditModal from './QuizEditModal';
import EditQuestionList from './EditQuestionList';


const EditQuiz = ({match, history}) => {
    const dispatch = useDispatch(),
        {id, title, description, questions} = useSelector(getQuizSelecor),
        [modalOpen, setModalOpen] = useState(false),
        [pageLoading, setPageLoading] = useState(true),
        [popUpSuccess, setPopUpSuccess] = useState(false),
        [popUpError, setPopUpError] = useState(false);

    useEffect(() => {
        if (match && match.params && match.params.quizId){
            setPageLoading(true)
            dispatch(getQuiz(match.params.quizId))
                .then(() => {
                    setPageLoading(false);
                })
                .catch(() => {
                    history.push('/your-quizzes');
                });
        }
    }, [match.params.quizId]);

    const openModal = () => {
        setModalOpen(true);
    }

    const closeModal = () => {
        setModalOpen(false);
    }

 
    const popUp = (
            <PopUpMessageContainer bottom='0' left='0'>
                {popUpSuccess ? <PopUpMessage messageType="success" handleClose={() => { setPopUpSuccess(false) }}>update successfull</PopUpMessage> : null}
                {popUpError ? <PopUpMessage messageType="error" handleClose={() => { setPopUpError(false) }}>Error something whent wrong, try again later</PopUpMessage> : null}
            </PopUpMessageContainer>
        );

    if (pageLoading) return <Box><Type h3 color="inverse" marginTopOff >Loading...</Type></Box>

    const handleCreateQuestion = () => {
        history.push(`/create-question/${id}/${questions.length}`)
    }

    return (
        <React.Fragment>
            <Box>
                <Type h2 color="inverse" marginTopOff>Edit Quiz</Type>
                <GridContainer>
                    <Grid>
                        <GridItem paddingOff xs='12'>
                            <InfoBlock 
                                title="Title"
                                body={title || <Type span color="placeholder">Select edit to add a title</Type>}
                                action={openModal}
                            />
                        </GridItem>
                        <GridItem paddingOff xs='12' marginTop="20px">
                            <InfoBlock
                                title={"descripton"}
                                body={description || <Type span color="placeholder">Select edit to add a description</Type>}
                                action={openModal}
                            />
                        </GridItem>
                        <GridItem paddingOff xs='12' marginTop="20px">
                            <Type h3 color="inverse">Select a question type and to create a new question</Type>
                        </GridItem>
                        <GridItem paddingOff xs="12" marginTop="8px">
                            <Button primary onClick={handleCreateQuestion}>Create Question</Button>
                        </GridItem>
                    </Grid>
                </GridContainer>      
            </Box>
            <Box>
                <EditQuestionList questions={questions}/>
            </Box>
            <QuizEditModal modalOpen={modalOpen} handleClose={closeModal} showSuccess={setPopUpSuccess} showError={setPopUpError}/>
            {(popUpSuccess || popUpError) ? popUp: null }
        </React.Fragment>
    );
}

export default EditQuiz;