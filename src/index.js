import React from 'react';
import ReactDOM from 'react-dom';
import './scss/main.scss';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '@store/reducers'
import ReduxThunk from 'redux-thunk';
import App from './App';
import {ThemeProvider} from 'styled-components'
import Theme from './theme';
// import * as serviceWorker from './serviceWorker';

const store = createStore(rootReducer, compose(applyMiddleware(ReduxThunk), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()))

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={Theme}>
      <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
