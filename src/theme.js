const white = "#ffffff",
    green = "#00AD30",
    yellow = "#CEBA00",
    red = "#D70000",
    slate = "#6493a9",
    primary = "#DF11F1",
    primaryLight = "#f37aff",
    primaryDark = "#B514C2",
    secondary = "#30A6E8",
    secondaryLight = "#34B6FF",
    secondaryDark = "#2788BE",
    textOnPrimary = white,
    textOnSecondary = white,
    textDefault = "#333333",
    textInverse = white,
    placeHolderColor = "#888888",
    borderColor = "#888888",
    defaultButtonColor = "#777777",
    errorColor = "#ff0000";

export default {
    primary,
    primaryLight,
    primaryDark,
    secondary,
    secondaryLight,
    secondaryDark,
    textOnPrimary,
    textOnSecondary,
    textDefault,
    textInverse,
    errorColor,
    defaultButtonColor,

    green,
    yellow,
    red,
    slate,
    
    success: green,
    warning: yellow,
    error: red,
    info: secondaryDark,

    bgPrimary: secondary,
    bgSecondary: white,

    placeHolderColor,
    borderColor,

    hPadding: 12,
    vPadding: 24,
    inputHPadding: 12,
    inputVPadding: 8,
    buttonHPadding: 16,
    buttonVPadding: 8,

    padding: 12,

    // baseFontSize is used to set the rem units
    baseFontSize: 16,

    borderRadius: "4px",
    boxShadow: "2px 2px 4px 0 rgba(0,0,0, .25)"
}