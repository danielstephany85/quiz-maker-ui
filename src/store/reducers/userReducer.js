import { 
    SET_USER, 
    SET_TOKEN, 
    SET_SIGNED_IN,
    LOGOUT
} from '../actions/userActions';

const initialState = {
    signedIn: false,
    token: ""
}

function userReducer(state = initialState, action) {
    switch (action.type) {
        case SET_USER:
            return {...state, ...action.user}
        case SET_SIGNED_IN:
            return { ...state, signedIn: action.signedIn }
        case SET_TOKEN:
            return {...state, token: action.token}
        case LOGOUT: 
            return {signedIn: false, token: ""}
        default:
            return state
    }
}

export default userReducer;