import { SET_QUIZ, UPDATE_QUIZ } from '../actions/quizActions';

function quizReducer(state = {}, action){
    switch(action.type){
        case SET_QUIZ:
            return {...action.quiz}
        case UPDATE_QUIZ: 
            const {title, description} = action.quiz;
            return {
                ...state,
                title,
                description
            }
        default:
            return state
    }
}

export default quizReducer;