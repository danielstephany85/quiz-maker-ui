import config from '../../config';

export const SET_USER = "SET_USER";
export const SET_SIGNED_IN = "SET_SIGNED_IN";
export const SET_TOKEN = "SET_TOKEN";
export const LOGOUT = "LOGOUT";

export function setUser(user){
    return { type: SET_USER, user }
}

export function setSignedIn(isSignedIn){
    return { type: SET_SIGNED_IN, signedIn: isSignedIn}
}

export function setToken(token) {
    return { type: SET_TOKEN, token}
}

export function logout() {
    return { type: LOGOUT }
}

export function logInUser(data){
    return (dispatch, getState) => {      
        return new Promise((resolve, reject) => {
            fetch(`${config.baseUrl}/log-in`, {
                method: 'POST',
                mode: 'cors',
                body: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'},
            })
                .then(res => res.json())
                .then(json => {
                    console.log(json);
                    if(json.status === "success" && json.data){
                        dispatch(setUser(json.data.user))
                        dispatch(setSignedIn(true));
                        dispatch(setToken(json.data.token));
                        window.localStorage.setItem("userToken", json.data.token);
                    }
                    return resolve(json);
                })
                .catch(e => {
                    console.log(e);
                    reject(e)
                });
        });
    }
}

export function getUserWithToken(token) {
    return dispatch => {
        return new Promise((resolve, reject) => {
            fetch(`${config.baseUrl}/users`, {
                mode: 'cors',
                headers: { 
                    'x-access-token': token,
                    'Content-Type': 'application/json' 
                },
            })
                .then(res => res.json())
                .then(json => {
                    if (json.status === "success" && json.data) {
                        dispatch(setUser(json.data.user))
                        dispatch(setSignedIn(true));
                        dispatch(setToken(token));
                        return resolve(json);
                    }
                    return reject(json);
                })
                .catch(e => {
                    reject(e)
                });
        });
    }
}

export const logoutUser = () => dispatch => new Promise(resolve => {
    window.localStorage.setItem("userToken", "");
    dispatch(logout());
    resolve();
})