import config from '../../config';

export const SET_QUIZ = 'SET_QUIZ';
export const UPDATE_QUIZ = 'UPDATE_QUIZ';

export function setQuiz(quiz){
    return {type: SET_QUIZ, quiz}
}

export function setUpdatedQuiz(quiz) {
    return { type: UPDATE_QUIZ, quiz }
}

export const  createQuiz = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    if (!data.title) return reject("title is required");
    fetch(`${config.baseUrl}/quizzes`, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json',
            'x-access-token': getState().user.token
        }
    })
    .then(res => res.json())
    .then(json => {
        if (json && json.data && json.data.quiz) {
            dispatch(setQuiz(json.data.quiz));
            return resolve(json.data.quiz)
        }
        return reject('unknown data error');
    })
    .catch(e => reject(e))
});


export const getQuiz = (id) => (dispatch, getState) => new Promise((resolve, reject) =>{
    fetch(`${config.baseUrl}/quizzes/${id}`, {
        mode: 'cors',
        headers: {
            'content-type': 'application/json',
            'x-access-token': getState().user.token
        }
    })
        .then(res => res.json())
        .then(json => {
            if(json.data && json.data.quiz){
                dispatch(setQuiz(json.data.quiz));
                return resolve(json.data.quiz)
            }
            return reject('unknown data error');
        })
        .catch(e => reject(e));
});


export const getQuizzes = () => (dispatch, getState) => new Promise((resolve, reject) => {
    fetch(`${config.baseUrl}/quizzes`, {
        mode: 'cors',
        headers: {
            'content-type': 'application/json',
            'x-access-token': getState().user.token
        }
    })
        .then(res => res.json())
        .then(json => {
            if(json.data && json.data.quizzes){
                return resolve(json)
            }
            return reject('unknown data error')
        })
        .catch(e => reject(e))
});


export const updateQuiz = (id, data) => (dispatch, getState) => new Promise((resolve, reject) => {
    if(!data.title) return reject('title is required');

    fetch(`${config.baseUrl}/quizzes/${id}`, {
        method: 'PUT',
        mode: 'cors',
        headers: {
            'content-type': 'application/json',
            'x-access-token': getState().user.token
        },
        body: JSON.stringify(data)
    })
        .then(res => res.json())
        .then(json => {
            if(json.status === 'success'){
                dispatch(setUpdatedQuiz(data));
                return resolve(json);
            }
            return reject();
        })
        .catch(e => reject(e));
});

export const saveQuestion = (data) => (dispatch, getState) => new Promise((resolve, reject) => {
    if(!data) return 'saveQuestion expects paramater of "data"';

    fetch(`${config.baseUrl}/questions/create`,{
        method: "POST",
        mode: 'cors',
        headers: {
            'content-type': 'application/json',
            'x-access-token': getState().user.token
        },
        body: JSON.stringify(data)
    })
    .then(res => res.json())
    .then(json => {
        if(json && json.status === 'success'){
            console.log(json)
            return resolve(json.data)
        }
    })
    .catch(e => {
        return reject(e)
    })

})