export const getUser = store => store.user

export const getSigned = store => store.user.signedIn
