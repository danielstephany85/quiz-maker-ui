export const hasValidInputs = (values, elementRefs, setErrors) => {
    let allValuesValid = true,
        errorValues = {};

    for (const key of Object.keys(values)) {
        if (!elementRefs[key].current.validity.valid) {
            errorValues[key] = `a valid ${key} is required`;
            allValuesValid = false;
        }
    }
    setErrors(errorValues);
    return allValuesValid;
};
