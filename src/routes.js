import React from 'react';
import { Switch, Redirect} from "react-router-dom";
import SafeRoute from '@components/elements/SafeRoute';

import Home from "@views/Home";
import SignUp from "@views/SignUp";
import SignIn from "@views/SignIn";

import YourQuizzes from '@views/YourQuizzes';
import CreateQuiz from '@views/CreateQuiz';
import Settings from '@views/Settings';
import YourInfo from '@views/YourInfo';
import Groups from '@views/Groups';
import EditQuiz from '@views/EditQuiz';
import EditQuestion from '@views/EditQuestion';

export const LoggedOutRoutes = props => {

    return (
        <Switch>
            <SafeRoute exact path="/sign-up" component={props => <SignUp {...props} />} />
            <SafeRoute exact path="/sign-in" component={props => <SignIn {...props} />} />
            <SafeRoute path="/" component={props => <Home {...props} />} />
        </Switch>
    )
} 

export const LoggedInRoutes = props => {

    return (
        <Switch>
            <SafeRoute exact path="/your-quizzes" component={props => <YourQuizzes {...props} />} />
            <SafeRoute exact path="/create-quiz" component={props => <CreateQuiz {...props} />} />
            <SafeRoute exact path="/settings" component={props => <Settings {...props} />} />
            <SafeRoute exact path="/your-info" component={props => <YourInfo {...props} />} />
            <SafeRoute exact path="/your-groups" component={props => <Groups {...props} />} />
            <SafeRoute exact path="/edit-quiz/:quizId" component={props => <EditQuiz {...props} />} />
            <SafeRoute exact path="/create-question/:quizId/:order" component={props => <EditQuestion {...props} />} />
            <SafeRoute exact path="/edit-question/:id" component={props => <EditQuestion {...props} />} />
            <SafeRoute path="/" component={props => <Redirect to="/your-quizzes" />} />
        </Switch>
    )
}
