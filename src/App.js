import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { HashRouter } from "react-router-dom";
import SiteConstraint from '@components/elements/SiteConstraint';
import SideNav from '@components/interfaces/SideNav';
import MainContent from '@components/elements/MainContent';
import {getSigned, getUser} from "@store/selectors/userSelectors";
import { getUserWithToken } from '@store/actions/userActions';
import {LoggedInRoutes, LoggedOutRoutes} from './routes';
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

function App() {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const signedIn = useSelector(getSigned);
  const user = useSelector(getUser);

  useEffect(() => {
    const token = window.localStorage.getItem("userToken");
    if (token) {
      setLoading(true);
      dispatch(getUserWithToken(token))
      .then(() => {
        setLoading(false);
      }).catch(() => {
        setLoading(false);
      });
    }
  }, [dispatch])

  const routes = signedIn ? <LoggedInRoutes history={history} /> : <LoggedOutRoutes history={history} />;

  return (
    <SiteConstraint>
      <HashRouter history={history}>
          { signedIn ? <SideNav userName={`${user.firstName} ${user.lastName}`}></SideNav> : null}
        <MainContent>
          <HashRouter>
            {loading ? null : routes}
          </HashRouter>
        </MainContent>
      </HashRouter>
    </SiteConstraint>
  );
}

export default App;
