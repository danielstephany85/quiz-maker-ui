import React from 'react';

const SelectOptions = props => (
    <>
        <option value=""></option>
        <option value="1">option 1</option>
        <option value="2">option 2</option>
    </>
);

export default SelectOptions;