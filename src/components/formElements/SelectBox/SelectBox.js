import React from 'react';
import ErrorTextSpacer from '@components/formElements/ErrorTextSpacer';
import InputWrapper from '@components/formElements/InputWrapper';
import BaseSelect from '@components/formElements/BaseSelect';
import ErrorText from '@components/formElements/ErrorText';
import PropTypes from 'prop-types';

const SelectBox = React.forwardRef(function selectBox({ className, error, fullWidth, ...others }, ref) {

    return (
        <InputWrapper className={className} fullWidth={fullWidth}>
            <BaseSelect fullWidth {...others} ref={ref} />
            {error ? <ErrorText>{error}</ErrorText> : <ErrorTextSpacer />}
        </InputWrapper>
    );
});


SelectBox.propTypes = {
    label: PropTypes.string.isRequired,
    error: PropTypes.string,
    fullWidth: PropTypes.bool,
    labelSet: PropTypes.bool,
    solid: PropTypes.bool,
    inverse: PropTypes.bool,
    required: PropTypes.bool,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

SelectBox.defaultProps = {
    fullWidth: false,
    labelSet: false,
    solid: false,
    inverse: false,
    required: false,
    focusColor: "secondary"
}

export default SelectBox;