import React from 'react';
import ErrorTextSpacer from '@components/formElements/ErrorTextSpacer';
import InputWrapper from '@components/formElements/InputWrapper';
import BaseInput from '@components/formElements/BaseInput';
import ErrorText from '@components/formElements/ErrorText';
import PropTypes from 'prop-types';

const TextBox = React.forwardRef(function textBox({ className, error, fullWidth, ...others }, ref){

    return (
        <InputWrapper className={className} fullWidth={fullWidth}>
            <BaseInput fullWidth {...others} ref={ref}/>
            {error ? <ErrorText>{error}</ErrorText> : <ErrorTextSpacer />}
        </InputWrapper>
    );
});


TextBox.propTypes = {
    label: PropTypes.string.isRequired,
    error: PropTypes.string,
    fullWidth: PropTypes.bool,
    labelSet: PropTypes.bool,
    solid: PropTypes.bool,
    inverse: PropTypes.bool,
    required: PropTypes.bool,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

TextBox.defaultProps = {
    fullWidth: false,
    labelSet: false,
    solid: false,
    inverse: false,
    required: false,
    focusColor: "secondary"
}

export default TextBox;