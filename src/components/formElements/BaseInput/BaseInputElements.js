import styled from 'styled-components';
import WrappedLabelText from '@components/formElements/WrappingLabelText';

export const IconElement = styled.span`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    font-size: 1.125rem;
    width: 32px;
    color: ${props => props.inverse ? "#fff" : props.theme.placeHolderColor};
    position: absolute;
    bottom: 0;
    top: 0;
    right: 0;
`;

export const Input = styled.input`
    border-radius: ${props => props.solid ? props.theme.borderRadius : '0px'};
    border-style: solid;
    border-width: ${props => props.solid ? "2px" : '0 0 2px 0'};
    border-color: ${props => props.inverse ? "#fff" : props.theme.borderColor};
    background-color: ${props => props.solid ? props.theme.bgSecondary : 'transparent'};
    width: 100%;
    height: 38px;
    padding: ${props => props.solid ? (props.theme.padding / 2) + "px" : `${(props.theme.padding / 2)}px 0`};
    font-size: 1.125rem;
    color: ${props => (props.inverse && !props.solid) ? "#fff" : props.theme.textDefault};
    font-weight: 400;
    position: relative;
    z-index: 2;
    &:focus {
        outline: none;
        border-color: ${props => props.theme[props.focusColor]};
    }
    &:focus + ${WrappedLabelText} {
        font-size: .75rem;
        bottom: 100%;
        color: ${props => props.inverse ? "#fff" : props.theme.textDefault};
    }
`;


