import React, { Component } from 'react';
import { IconElement, Input } from './BaseInputElements'
import WrappingLabel from '@components/formElements/WrappingLabel';
import WrappingLabelText from '@components/formElements/WrappingLabelText';
import PropTypes from 'prop-types';

class BaseInput extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: this.props.value || ''
        }
    }

    componentDidUpdate(){
        if(!this.state.value && this.props.value){
            this.setState({value: this.props.value});
        }   
    }
    
    handleChange = (e) => {
        this.setState({value: e.target.value});

        if (this.props.onChange) this.props.onChange(e);
    }

    render() {
        const { className, label, onChange, fullWidth, maxWidth, labelSet, innerRef, inverse, solid, iconClass, ...others} = this.props;
        let classes = className ? className : '';
        let hasIcon = !!iconClass 
        
        if (this.state.value || labelSet){
            classes += " active";
        }

    
        return (
            <WrappingLabel fullWidth={fullWidth} maxWidth={maxWidth} inverse={inverse} hasIcon={hasIcon} solid={solid} className={classes}>
                <Input inverse={inverse} solid={solid} onChange={this.handleChange}  {...others} ref={innerRef} />
                <WrappingLabelText inverse={inverse} solid={solid} >{label}{others.required ? <sup>*</sup> : null}</WrappingLabelText>
                {iconClass ? <IconElement inverse={inverse} className={iconClass}></IconElement> : null}
            </WrappingLabel>
        );
    }
}


BaseInput.propTypes = {
    label: PropTypes.string.isRequired,
    error: PropTypes.string,
    fullWidth: PropTypes.bool,
    labelSet: PropTypes.bool,
    solid: PropTypes.bool,
    inverse: PropTypes.bool,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

BaseInput.defaultProps = {
    fullWidth: false,
    labelSet: false,
    solid: false,
    inverse: false,
    focusColor: "secondary"
}

export default React.forwardRef((props, ref) => <BaseInput {...props} innerRef={ref} /> );