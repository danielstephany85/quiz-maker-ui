import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const baseRadioButton = ({label, className, ...others}) => {

    return (
        <label className={className}>
            <span className="base-checkbox_label-text">{label}</span>
            <div className="base-checkbox_control">
                <input type="radio" {...others}/>
                <div className="base-checkbox_target"></div>
            </div>
        </label>
    );
}

const BaseRadioButton = styled(baseRadioButton)`
    display: inline-flex;
    align-items: center;
    cursor: pointer;
    span {
        display: inline-block;
    }
    input {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
    }
    .base-checkbox_control {
        display: inline-block;
        order: -1;
        margin-right: 6px;
        height: 22px;
        width: 22px;
        position: relative;
        ${props => props.flip ?
            `order: 1;
            margin-left: 6px`
            : 
            ""
        }
    }
    .base-checkbox_target  {
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        background-color: ${props => props.theme.bgSecondary};
        height: inherit;
        width: inherit;
        cursor: pointer;
        &::after {
            display: none;
        }
    }
    input:checked + .base-checkbox_target {
        &::after {
            background-color: ${props => props.theme.secondary};
            border-radius: 50%;
            display: inline-block;
            content: "";
            width: 16px;
            height: 16px;
        }
    }
`;

BaseRadioButton.propTypes = {
    flip: PropTypes.bool,
    label: PropTypes.string
}

export default BaseRadioButton;

