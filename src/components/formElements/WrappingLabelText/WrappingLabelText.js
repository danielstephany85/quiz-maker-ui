import styled from 'styled-components';
import PropTypes from 'prop-types';

export const WrappingLabelText = styled.span`
    margin-bottom: 2px;
    position: absolute;
    bottom: ${props => props.solid ? "6px" : "4px"};
    left: ${props => props.solid ? "6px" : "0"};
    font-size: 1.125rem;
    color: ${props => (props.inverse && !props.solid) ? "#fff" : props.theme.placeHolderColor};
    z-index: 2;
    transition: 0.25s ease;
`;

WrappingLabelText.propTypes = {
    solid: PropTypes.bool,
    inverse: PropTypes.bool,
}