import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const baseCheckbox = ({label, className, ...others}) => {

    return (
        <label className={className}>
            <span className="base-checkbox_label-text">{label}</span>
            <div className="base-checkbox_control">
                <input type="checkbox" {...others}/>
                <div className="base-checkbox_target"></div>
            </div>
        </label>
    );
}

const BaseCheckbox = styled(baseCheckbox)`
    display: inline-flex;
    align-items: center;
    cursor: pointer;
    span {
        display: inline-block;
    }
    input {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
    }
    .base-checkbox_control {
        display: inline-block;
        order: -1;
        margin-right: 6px;
        height: 22px;
        width: 22px;
        position: relative;
        ${props => props.flip ?
            `order: 1;
            margin-left: 6px`
            : 
            ""
        }
    }
    .base-checkbox_target  {
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border-radius: ${props => props.theme.borderRadius};
        background-color: ${props => props.theme.bgSecondary};
        height: inherit;
        width: inherit;
        cursor: pointer;
        &::after {
            display: none;
        }
    }
    input:checked + .base-checkbox_target {
        &::after {
            display: inline-block;
            content: "";
            width: 10px;
            height: 17px;
            margin-bottom: 2px;
            border-right: 4px solid ${props => props.theme.secondary};
            border-bottom: 4px solid ${props => props.theme.secondary};
            transform: rotate(35deg);
        }
    }

`;

BaseCheckbox.propTypes = {
    flip: PropTypes.bool,
    label: PropTypes.string
}

export default BaseCheckbox;

