import styled from 'styled-components'

export const ErrorText = styled.span`
    display: block;
    margin-top: 0px;
    color: ${props => props.theme.errorColor};
    font-size: .75rem;
    font-weight: 500;
    line-height: .75rem;
    letter-spacing: .8px;
    height: 12px;
`;