import styled from 'styled-components';
import PropTypes from 'prop-types';

export const LabelText = styled.span`
    margin-bottom: 2px;
    position: absolute;
    bottom: calc(100% - 28px);
    left: 8px;
    font-size: 1.125rem;
    color: ${props => props.theme.placeHolderColor};
    z-index: 2;
    transition: 0.25s ease;
`;

export const Textarea = styled.textarea`
    border-radius: ${props => props.theme.borderRadius};
    border: 2px solid ${props => props.inverse ? props.theme.bgSecondary : props.theme.borderColor};
    background-color: ${props => (props.inverse || props.solid) ? props.theme.bgSecondary : 'transparent'};
    width: 100%;
    min-height: ${props => props.minHeight};
    padding: ${props => props.theme.padding / 2}px;
    font-size: 1.125rem;
    color: ${props => props.theme.textDefault};
    font-weight: 400;
    letter-spacing: .05rem;
    line-height: 1.3;
    position: relative;
    resize: none;
    z-index: 2;
    &:focus {
        outline: none;
        border-color: ${props => props.theme[props.focusColor]}
    }
    &:focus + ${LabelText} {
        font-size: .75rem;
        bottom: 100%;
        left: 0;
        color: ${props => props.inverse ? "#fff" : props.theme.textDefault};
    }
`;

Textarea.propTypes = {
    solid: PropTypes.bool,
    inverse: PropTypes.bool,
    minHeight: PropTypes.string,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

Textarea.defaultProps = {
    minHeight: "120px",
    focusColor: "secondary"
}

export const Label = styled.label`
    display: inline-block;
    position: relative;
    margin: 16px 0 2px;
    width: ${props => props.fullWidth ? "100%" : "auto"};
    min-width: 200px;
    max-width: ${props => props.maxWidth ? props.maxWidth : "100%"};
    font-size: 1rem;
    &.active {
        ${LabelText} {
            font-size: .75rem;
            bottom: 100%;
            left: 0;
            color: ${props => props.inverse ? "#fff" : props.theme.textDefault};
        }
    }
`;

Label.propTypes = {
    fullWidth: PropTypes.bool
}

