import React, { Component } from 'react';
import { LabelText, Textarea, Label } from './BaseTextareaElements'
import PropTypes from 'prop-types';

class BaseInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value || ''
        }
    }

    componentDidUpdate() {
        if (!this.state.value && this.props.value) {
            this.setState({ value: this.props.value });
        }
    }

    handleChange = (e) => {
        this.setState({ value: e.target.value });

        if (this.props.onChange) this.props.onChange(e);
    }

    render() {
        const { className, label, onChange, fullWidth, maxWidth, labelSet, innerRef, inverse, ...others } = this.props;
        let classes = className ? className : '';

        if (this.state.value || labelSet) {
            classes += " active";
        }

        return (
            <Label fullWidth={fullWidth} maxWidth={maxWidth} inverse={inverse} className={classes}>
                <Textarea inverse={inverse} onChange={this.handleChange}  {...others} ref={innerRef} />
                <LabelText inverse={inverse} >{label}{others.required ? <sup>*</sup> : null}</LabelText>
            </Label>
        );
    }
}


BaseInput.propTypes = {
    label: PropTypes.string.isRequired,
    error: PropTypes.string,
    fullWidth: PropTypes.bool,
    labelSet: PropTypes.bool,
    minHeight: PropTypes.string,
    inverse: PropTypes.bool,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

BaseInput.defaultProps = {
    fullWidth: false,
    labelSet: false,
    inverse: false,
    focusColor: "secondary"
}

export default React.forwardRef((props, ref) => <BaseInput {...props} innerRef={ref} />);