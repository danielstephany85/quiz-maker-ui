import styled from 'styled-components';
import PropTypes from 'prop-types';

export const Form = styled.form`
    display: flex;
    flex-wrap: wrap;
    width: calc(100% + ${props => props.offsetPadding ? props.theme.hPadding * 2 : 0}px);
    margin-left: -${props => props.offsetPadding ? props.theme.hPadding : 0}px;
    margin-right: -${props => props.offsetPadding ? props.theme.hPadding : 0}px;
`;

Form.propTypes = {
    offsetPadding: PropTypes.bool
}