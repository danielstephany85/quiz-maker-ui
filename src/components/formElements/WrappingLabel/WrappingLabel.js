import styled from 'styled-components';
import PropTypes from 'prop-types';
import WrappingLabelText from '@components/formElements/WrappingLabelText';

export const WrappingLabel = styled.label`
    display: inline-block;
    position: relative;
    margin: 18px 0 2px;
    width: ${props => props.fullWidth ? "100%" : "auto"};
    min-width: 200px;
    max-width: ${props => props.maxWidth};
    font-size: 1rem;
    &.active {
        ${WrappingLabelText} {
            font-size: .75rem;
            bottom: 100%;
            left: ${props => props.solid ? "6px" : "0"};
            color: ${props => props.inverse ? "#fff" : props.theme.textDefault};
        }
    }
`;

WrappingLabel.propTypes = {
    fullWidth: PropTypes.bool,
    maxWidth: PropTypes.string,
    inverse: PropTypes.bool,
    solid: PropTypes.bool
}

WrappingLabel.defaultProps = {
    maxWidth: "100%"
}