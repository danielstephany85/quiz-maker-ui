import React, {Component} from 'react';
import PropTypes from "prop-types";
import {
    WrappingLabel,
    LabelText,
    Button,
    Input,
    LoadingSpan,
} from './InputWithButtonElements';


class InputWithButton extends Component {    
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    handleChange = (e) => {
        this.setState({ value: e.target.value });

        if (this.props.onChange) this.props.onChange(e);
    }

    render(){
        const {loading, label, buttonText, buttonAction, labelSet, className, innerRef, onChange, ...others} = this.props
        let classes = className;

        if (labelSet || this.state.value) {
            classes += " active";
        }

        const loadingEl = loading ? <LoadingSpan><span></span></LoadingSpan> : null

        return (
            <WrappingLabel className={classes}>
                <Input onChange={this.handleChange} ref={innerRef} {...others}/>
                <LabelText>{label}</LabelText>
                <Button type="submit" className={loading ? 'loading' : ''} >{buttonText}{loadingEl}</Button>
            </WrappingLabel>
        )
    }
}

InputWithButton.propTypes = {
    label: PropTypes.string.isRequired,
    buttonText: PropTypes.string.isRequired,
    buttonAction: PropTypes.func,
    labelSet: PropTypes.bool,
    loading: PropTypes.bool
}

export default React.forwardRef((props, ref) => <InputWithButton {...props} innerRef={ref} />);