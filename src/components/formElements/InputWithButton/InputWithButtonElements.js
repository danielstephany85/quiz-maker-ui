import styled, { keyframes }  from 'styled-components';

export const LabelText = styled.span`
    margin-bottom: 0;
    position: absolute;
    bottom: ${props => props.theme.inputVPadding - 1}px;
    left: ${props => props.theme.inputHPadding}px;
    font-size: 1.125rem;
    color: ${props => props.theme.placeHolderColor};
    z-index: 1;
    transition: 0.25s ease;
`;

export const Input = styled.input`
    border-top-left-radius: ${props => props.theme.borderRadius};
    border-bottom-left-radius: ${props => props.theme.borderRadius};
    border: none;
    padding: ${props => props.theme.inputVPadding + 'px ' + props.theme.inputHPadding + 'px'};
    flex-grow: 1;
    &:focus {
        outline: none;
    }
    &:focus + ${LabelText} {
        bottom: calc(100% + 2px);
        left: 0;
        color: ${props => props.theme.bgSecondary};
    }
`;

export const Button = styled.button`
    border-top-right-radius: ${props => props.theme.borderRadius};
    border-bottom-right-radius: ${props => props.theme.borderRadius};
    background-color: ${props => props.theme.primary};
    border: none;
    padding: ${props => props.theme.buttonVPadding + 'px ' + props.theme.buttonHPadding + 'px'};
    color: #fff;
    font-weight: 500;
    position: relative;
    &.loading {
        color: transparent;
    }
    &:focus {
        outline: none;
        text-decoration: underline;
    }
`;

export const WrappingLabel = styled.label`
    display: flex;
    width: 100%;
    margin-top: ${props => props.theme.baseFontSize * 1.5}px;
    position: relative;
    &.active {
        ${LabelText} {
            bottom: calc(100% + 2px);
            left: 0;
            color: ${props => props.theme.bgSecondary};
        }
    }
`;

const buttonLoad = keyframes`
    0% {
        opacity: 0;
        transform: translateX(calc(-100% + 4px));
    }
    10% {
        opacity: 1;
        transform: translateX(calc(-100% + 4px));
    }
    90% {
        opacity: 1;
        transform: translateX(calc(100% - 4px));
    }
    100% {
        opacity: 0;
        transform: translateX(calc(100% - 4px));
    }
`;

export const LoadingSpan = styled.span`
    border-radius: 4px;
    display: flex;
    height: 4px;
    width: calc(100% - ${props => props.theme.padding * 2}px);
    position: absolute;
    top: calc(50% - 2px);
    left: ${props => props.theme.padding}px; 
    overflow: hidden;
    & > span {
        display: block;
        width: 100%;
        right: 100%;
        border-radius: 4px;
        background-color: ${props => props.theme.bgSecondary};
        animation: ${buttonLoad} 2s ease 0s infinite forwards;
        transform: translateX(calc(-100% + 4px))
        opacity: 0;
    }
`;