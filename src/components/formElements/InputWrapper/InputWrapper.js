import styled from 'styled-components';

const InputWrapper = styled.div`
    display: inline-block;
    width: ${props => props.fullWidth ? "100%" : "auto"};
    padding-left: ${props => props.theme.padding}px;
    padding-right: ${props => props.theme.padding}px;
    vertical-align: middle;
    margin-bottom: 8px;
`;

export default InputWrapper;