import styled from 'styled-components';

const ErrorTextSpacer = styled.div`
    display: block;
    height: 12px;
`;

export default ErrorTextSpacer;