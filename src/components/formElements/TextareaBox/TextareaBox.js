import React from 'react';
import ErrorTextSpacer from '@components/formElements/ErrorTextSpacer';
import InputWrapper from '@components/formElements/InputWrapper';
import BaseTextarea from '@components/formElements/BaseTextarea';
import ErrorText from '@components/formElements/ErrorText';
import PropTypes from 'prop-types';

const TextareaBox = React.forwardRef(function textareaBox({ className, error, fullWidth, ...others }, ref) {

    return (
        <InputWrapper className={className} fullWidth={fullWidth}>
            <BaseTextarea fullWidth {...others} ref={ref} />
            {error ? <ErrorText>{error}</ErrorText> : <ErrorTextSpacer />}
        </InputWrapper>
    );
});


TextareaBox.propTypes = {
    label: PropTypes.string.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    fullWidth: PropTypes.bool,
    labelSet: PropTypes.bool,
    minHeight: PropTypes.string,
    inverse: PropTypes.bool,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

TextareaBox.defaultProps = {
    required: false,
    fullWidth: false,
    labelSet: false,
    inverse: false,
    minHeight: "120px",
    focusColor: "secondary"
}

export default TextareaBox;