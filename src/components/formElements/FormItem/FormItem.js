import styled from 'styled-components';
import PropTypes from 'prop-types';
import GridItem from '@components/elements/GridItem';

export const FormItem = styled(GridItem)`
`;

const sizeOptions = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];

FormItem.propTypes = {
    justifyContent: PropTypes.oneOf(["flex-start", "flex-end", "center", "space-between", "space-around", "space-evenly"]),
    paddingOff: PropTypes.bool,
    marginTop: PropTypes.string,
    marginBottom: PropTypes.string,
    xs: PropTypes.oneOf(sizeOptions),
    sm: PropTypes.oneOf(sizeOptions),
    md: PropTypes.oneOf(sizeOptions),
    lg: PropTypes.oneOf(sizeOptions),
    xg: PropTypes.oneOf(sizeOptions),
}

FormItem.defaultProps = {
    justifyContent: "flex-start",
    marginTop: "0px",
    // marginBottom: "8px",
    paddingOff: true
}