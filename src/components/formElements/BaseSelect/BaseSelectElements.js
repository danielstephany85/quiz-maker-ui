import styled from 'styled-components';
import WrappedLabelText from '@components/formElements/WrappingLabelText';
import PropTypes from 'prop-types';

export const SelectBackground = styled.div`
    background-color: ${props => props.solid ? props.theme.bgSecondary : 'transparent'};        
    display: inline-block;
    border-radius: ${props => props.solid ? props.theme.borderRadius : '0px'};
    width: 100%;
`;

SelectBackground.propTypes = {
    solid: PropTypes.bool
}

export const Select = styled.select`
    border-radius: ${props => props.solid ? props.theme.borderRadius : '0px'};
    border-style: solid;
    border-width: ${props => props.solid ? "2px"  : '0 0 2px 0'};
    border-color: ${props => props.inverse ? "#fff" : props.theme.borderColor};
    background-color: transparent;
    width: 100%;
    height: 38px;
    padding: ${props => props.solid ? (props.theme.padding / 2) + 'px' : "0"} 0;
    font-size: 1.125rem;
    color: ${props => (props.inverse && !props.solid) ? "#fff" : props.theme.textDefault};
    font-weight: 400;
    position: relative;
    z-index: 3;
    &:focus {
        outline: none;
        border-color: ${props => props.theme[props.focusColor]}
    }
    &:focus + ${WrappedLabelText} {
        font-size: .75rem;
        bottom: 100%;
        color: ${props => props.inverse ? "#fff" : props.theme.textDefault};
    }
`;

Select.propTypes = {
    solid: PropTypes.bool,
    inverse: PropTypes.bool,
}



