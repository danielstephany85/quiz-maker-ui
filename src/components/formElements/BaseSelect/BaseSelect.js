import React, { Component } from 'react';
import { Select, SelectBackground} from './BaseSelectElements'
import PropTypes from 'prop-types';
import WrappingLabel from '@components/formElements/WrappingLabel';
import WrappingLabelText from '@components/formElements/WrappingLabelText';

class BaseSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value || ''
        }
    }

    componentDidUpdate() {
        if (!this.state.value && this.props.value) {
            this.setState({ value: this.props.value });
        }
    }

    handleChange = (e) => {
        this.setState({ value: e.target.value });

        if (this.props.onChange) this.props.onChange(e);
    }

    render() {
        const { className, children, label, onChange, fullWidth, maxWidth, labelSet, innerRef, inverse, solid, ...others } = this.props;
        let classes = className ? className : '';

        if (this.state.value || labelSet) {
            classes += " active";
        }


        return (
            <WrappingLabel fullWidth={fullWidth} maxWidth={maxWidth} inverse={inverse} solid={solid} className={classes}>
                <SelectBackground solid={solid} >
                    <Select inverse={inverse} solid={solid} onChange={this.handleChange}  {...others} ref={innerRef} >{children}</Select>
                    <WrappingLabelText inverse={inverse} solid={solid} >{label}{others.required ? <sup>*</sup> : null}</WrappingLabelText>
                </SelectBackground>
            </WrappingLabel>
        );
    }
}


BaseSelect.propTypes = {
    required: PropTypes.bool,
    label: PropTypes.string.isRequired,
    error: PropTypes.string,
    fullWidth: PropTypes.bool,
    labelSet: PropTypes.bool,
    solid: PropTypes.bool,
    focusColor: PropTypes.oneOf(["primary", "secondary"])
}

BaseSelect.defaultProps = {
    required: false,
    fullWidth: false,
    labelSet: false,
    solid: false,
    focusColor: "secondary"
}

export default React.forwardRef((props, ref) => <BaseSelect {...props} innerRef={ref} />);