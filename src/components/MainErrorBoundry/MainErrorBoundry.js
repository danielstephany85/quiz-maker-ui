import React, {Component} from 'react';
import MessageBox from '@components/interfaces/MessageBox';
import Button from '@components/elements/Button';


class MainErrorBoundry extends Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        console.error(error, errorInfo);
    }

    handleReset = () => {
        this.props.history.push("/your-quizzes");
        this.setState({hasError: false})
    }

    render() {
        if (this.state.hasError) {
            return (
                <MessageBox
                    title="Oh noes..."
                    subtitle="Looks like there has been an error"
                    body="Please navigate back to the main site"
                    button={<Button primary onClick={this.handleReset}>Go To Your Quizzes</Button>}
                />
            );
        }
        return this.props.children; 
    }
}

export default MainErrorBoundry;