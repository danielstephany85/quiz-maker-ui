import React from 'react';
import { 
    Panel,
    TitleContainer,
    PanelSection,
    NavList,
    NavListItem,
    SideNavLink
} from './SideNavElements';

const SideNav = props => {

    return (
        <Panel>
            <TitleContainer>Quiz Maker</TitleContainer>
            <PanelSection>
                <SideNavLink to='/your-info' iconClasses="fas fa-user">{props.userName}</SideNavLink>
            </PanelSection>
            <nav>
                <PanelSection>
                    <NavList>
                        <NavListItem>
                            <SideNavLink to='/create-quiz' iconClasses="fas fa-feather">Create Quiz</SideNavLink>
                        </NavListItem>
                        <NavListItem>
                            <SideNavLink to='/your-quizzes' iconClasses="fas fa-copy">Your Quizes</SideNavLink>
                        </NavListItem>
                        <NavListItem>
                            <SideNavLink to='/your-groups' iconClasses="fas fa-users">groups</SideNavLink>
                        </NavListItem>
                    </NavList>
                </PanelSection>
                <PanelSection noBorder>
                    <SideNavLink to='/settings' iconClasses="fas fa-cog">Settings</SideNavLink>
                </PanelSection>
            </nav>
        </Panel>
    );
}

export default SideNav