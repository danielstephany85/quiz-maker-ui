import styled from 'styled-components';
import React from 'react';
import { NavLink } from 'react-router-dom';

export const Panel = styled.div`
    background-color: ${props => props.theme.bgSecondary};
    min-width: 300px;
`;

export const PanelSection = styled.section`
    padding: ${props => props.theme.vPadding}px 0;
    position: relative;
    &::after {
        content: '';
        display: ${props => props.noBorder ? "none" : "block"};
        background-color: ${props => props.theme.borderColor};
        height: 2px;
        width: calc(100% - ${props => props.theme.hPadding * 2}px);
        position: absolute;
        bottom: -1px;
        right: ${props => props.theme.hPadding}px;
    }
`;

export const TitleContainer = styled.h1`
    background-color: ${props => props.theme.primary};
    margin: 0;
    padding: ${props => props.theme.padding}px;
    font-family: 'Pacifico', cursive;
    font-size: 28px;
    color: ${props => props.theme.textOnPrimary};
    font-weight: 500;
`;

export const NavList = styled.ul`
    margin: 0;
    padding: 0px;
    list-style: none;
`;

export const NavListItem = styled.li`
    display: block;
`;



const sideNavLink = ({ className, iconClasses, children, ...others}) => {
    return (
        <NavLink className={className} {...others}>
            {iconClasses ? <i className={iconClasses}></i> : null}
            {children}
        </NavLink>
    );
}

export const SideNavLink = styled(sideNavLink)`
  display: block;
  padding: ${props => props.theme.vPadding}px ${props => props.theme.hPadding}px;
  font-size: 18px;
  text-decoration: none;
  color: ${props => props.theme.textDefault};
  transition: color 0.25s ease;
  &:hover {
      color: ${props => props.theme.secondaryDark};
  }
  &:hover i {
      color: ${props => props.theme.secondaryDark};
  }
    & > i {
        display: inline-block;
        color: ${props => props.theme.borderColor};
        margin-right: ${props => props.theme.padding}px;
        font-size: 24px;
        transition: color 0.25s ease;
    }
    &.active {
        color: ${props => props.theme.secondaryDark};
        i {
            color: ${props => props.theme.secondaryDark};
        }
    }
`;