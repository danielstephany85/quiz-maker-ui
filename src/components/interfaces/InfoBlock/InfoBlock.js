import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Sheet from '@components/elements/Sheet';

const infoBlock = ({ className, title, body, action}) => {

    return (
        <Sheet className={className}>
            <div className='info-block__header'>
                <h3>{title}</h3>
                <button onClick={action}>edit</button>
            </div>
            <div className='info-block__content'>{body}</div>
        </Sheet>
    );
}

const InfoBlock = styled(infoBlock)`
    padding: 0;
    .info-block__header {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        border-top-left-radius: ${props => props.theme.borderRadius};
        border-top-right-radius: ${props => props.theme.borderRadius};
        background-color: ${props => props.bgColor ? props.bgColor : props.theme.primary};
        min-height: 30px;
        padding: 0 ${props => props.theme.padding}px;
        h3 {
            flex-grow: 1;
            color: ${props => props.theme.textInverse};
            font-weight: 500;
            font-size: 14px;
            margin: 0;
        }
        button {
            background-color: transparent;
            border: none;
            font-weight: 500;
            font-size: 14px;
            color: ${props => props.theme.textInverse};
            padding: ${props => props.theme.padding / 2}px ${props => props.theme.padding}px;
            cursor: pointer;
            &:hover,
            &:focus {
                outline: none;
                text-decoration: underline;
            }
        }
    }
    .info-block__content {
        padding: ${props => props.theme.padding}px;
    }
`;

InfoBlock.propTypes = {
    /* text that is displayed in the header */
    title: PropTypes.string.isRequired, 
    /* text that is displayed below the header */
    body: PropTypes.string.isRequired, 
    /* the bacground color for the header */
    bgColor: PropTypes.string,
    /* the function that is called when the action button is clicked */
    action: PropTypes.func.isRequired
}

export default InfoBlock;