import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '@components/elements/Button';
import Type from '@components/elements/Type';

const TrueOrFalseCtrlEl = ({ className, trueAction, falseAction}) => {

    return (
        <div className={className}>
            <Button primary onClick={trueAction}>True</Button>
            <Type span color="inverse">– or –</Type>
            <Button primary onClick={falseAction}>False</Button>
        </div>
    );
}


const TrueOrFalseCtrl = styled(TrueOrFalseCtrlEl)`
    ${Button} {
        &:last-of-type {
            margin-left:  ${props => props.theme.padding}px;
        }
    }
    ${props => props.disabled ? `
        ${Button} {
            cursor: not-allowed;
            &:focus,
            &:hover {
                box-shadow: none;
                background-color: ${props.theme.primary};
                border-color: ${props.theme.primary};
            }
        }
    ` : ''}
`;

TrueOrFalseCtrl.propTypes = {
    trueAction: PropTypes.func,
    falseAction: PropTypes.func,
    disabled: PropTypes.bool
}


export default TrueOrFalseCtrl;