import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import TextLink from '@components/elements/TextLink';

const selectorBlock = ({className, quiz}) => {
    const {title, id} = quiz;
    return (
        <div className={className} >
            <h3>{title}</h3>
            <div className="selector-block__actions">
                <TextLink>Assign</TextLink>
                <TextLink component={Link} to={`edit-quiz/${id}`}>Edit</TextLink>
            </div>
        </div>
    );
}

const SelectorBlock = styled(selectorBlock)`
    display: flex;
    justify-content: space-between;
    background-color: ${props => props.theme.bgSecondary};
    border-radius: ${props => props.theme.borderRadius};
    padding: ${props => props.theme.padding}px;
    margin: ${props => props.theme.padding}px 0;
    h3 {
        font-weight: 400;
        font-size: ${props => props.theme.baseFontSize}px;
        margin: 0;
    }
    .elector-block__actions {

    }
    ${TextLink} {
        margin-right: ${props => props.theme.padding}px;
        &:last-child {
            margin-right: 0;
        }
    }
`;

export default SelectorBlock;