import React from 'react';
import Box from '@components/elements/Box';
import Sheet from '@components/elements/Sheet';
import Type from '@components/elements/Type';
import Grid from '@components/elements/Grid';
import GridItem from '@components/elements/GridItem';


function MessageBox({title, subtitle, body, button}){
    let buttonSection;
    if (button) {
        buttonSection = (
            <Grid>
                <GridItem paddingOff marginTop="20px" xs="12" align="center">
                    {button}
                </GridItem>
            </Grid>
        );
    }

    return (
        <Box maxWidth="600px">
            <Sheet align="center">
                {title ? <Type h2 color="primary">{title}</Type> : null}
                {subtitle ? <Type h3 >{subtitle}</Type> : null}
                {body ? <Type p >{body}</Type> : null}           
                {button ? buttonSection : null}
            </Sheet>
        </Box>
    );
}

export default MessageBox;