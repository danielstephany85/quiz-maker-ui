import styled from 'styled-components';

export const Logo = styled.h1`
    display: inline-block;
    font-family: 'Pacifico', cursive;
    color: ${props => props.theme.bgSecondary};
    font-size: 32px;
    letter-spacing: 2px;
    margin: 0;
    & > a {
        text-decoration: none;
        color: inherit;
    }
`;

export const Header = styled.header`
    display: flex;
    padding: ${props => props.theme.padding}px;
`;