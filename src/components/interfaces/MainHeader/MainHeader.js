import React from 'react';
import {Logo, Header} from './MainHeaderElements';
import {Link} from 'react-router-dom';

const MainHeader = props => {
    return (
        <Header>
            <Logo>
                <Link to="/">Quiz Maker</Link>
            </Logo>
        </Header>
    );
}

export default MainHeader;