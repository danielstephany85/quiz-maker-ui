import styled from 'styled-components';
import ErrorText from '@components/formElements/ErrorText';

export const QuestionOptionEditorWrap = styled.li`
    border-radius: ${props => props.theme.borderRadius};
    box-shadow: ${props => props.theme.boxShadow};
    background-color: ${props => props.theme.bgSecondary};
    padding: 0;
    list-style: none;
    & + & {
        margin-top: 12px;
    }
`;

export const Content = styled.div`
    display: flex;
    padding: ${props => props.theme.padding}px;
    padding-bottom: 0px;
`;

export const TextContainer = styled.div`
    flex-grow: 1;
    position: relative;
    margin-bottom: ${props => props.theme.padding}px;
    margin-left: ${props => props.theme.padding * 2}px;
    ${ErrorText} {
        position: absolute;
        top: 100%;
        left: 0;
    }
`;

export const Textarea = styled.textarea`
display: block;
    border: 2px solid transparent;
    border-radius: ${props => props.theme.borderRadius};
    height: auto;
    width: 100%;
    padding: 4px;
    resize: none;
    &:focus {
        outline: none;
    }
    ${props => props.error ? `
        border-color: ${props.theme.errorColor}
    ` : null}
`;

export const Footer = styled.div`
    border-radius: 0 0 ${props => `${props.theme.borderRadius} ${props.theme.borderRadius}`};
    display: flex;
    justify-content: space-between;
    width: 100%;
    background-color: ${props => props.theme.slate};
    padding: ${props => props.theme.padding / 2}px ${props => props.theme.padding}px;
    color: #fff;
`;
