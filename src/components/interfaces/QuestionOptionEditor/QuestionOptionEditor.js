import React, {useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import { QuestionOptionEditorWrap, Content, TextContainer, Textarea, Footer } from './QuestionOptionEditorElements';
import QuestionOptionButton from '@components/elements/QuestionOptionButton';
import TextButton from '@components/elements/TextButton'; 
import BaseRadioButton from '@components/formElements/BaseRadioButton';
import ErrorText from '@components/formElements/ErrorText';


const QuestionOptionEditor = ({ name, ctrlName, optionText, onChange, buttonText, markCorrect, handleDelete, error}) => {
    const textareaRef = useRef();

    useEffect(() => {
        const textarea = textareaRef.current;
        function OnInput() {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        }

        textarea.setAttribute('style', 'height:' + textarea.scrollHeight + 'px;overflow-y:hidden;');
        textarea.addEventListener("input", OnInput, false);

        return () => {textarea.removeEventListener("input", OnInput, false)}
    }, []);

    return (
        <QuestionOptionEditorWrap>
            <Content>
                <QuestionOptionButton secondary active outlined >{buttonText}</QuestionOptionButton> 
                <TextContainer>
                    <Textarea error={error} name={name} placeholder="Enter answer option text" onChange={onChange} value={optionText || ""} ref={textareaRef}></Textarea >
                    { error ? <ErrorText>{error}</ErrorText> : null }
                </TextContainer>
            </Content>
            <Footer>
                <BaseRadioButton name={ctrlName} label="Mark as correct option" onChange={markCorrect}/>
                <TextButton inverse onClick={handleDelete}>delete</TextButton>
            </Footer>
        </QuestionOptionEditorWrap>
    );
}

QuestionOptionEditor.propTypes = {
    /** Name attribute for the textarea */
    name: PropTypes.string.isRequired,
    /** Name attribute for the radio button */
    ctrlName: PropTypes.string.isRequired,
    /** Text that is provided as the value for the textarea */
    optionText: PropTypes.string,
    /** onChange event for the textarea */  
    onChange: PropTypes.func.isRequired,
    /** Text for the button to the left of the textarea */
    buttonText: PropTypes.string.isRequired,
    /** onChange for the radio button */
    markCorrect: PropTypes.func.isRequired,
    /** action for the delete button */
    handleDelete: PropTypes.func.isRequired,
    /** error text */
    error: PropTypes.string
}


export default QuestionOptionEditor;