import React, {useEffect} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Type from '@components/elements/Type';
import IconButton from '@components/elements/IconButton';

const PopUpMessageText = styled(Type)`
        flex-grow: 1;
        margin-top: 6px;
        margin-bottom: 0;
        font-weight: 500;
        font-size: 18px;
        padding-right: ${props => props.theme.padding}px;
`;

const PopUpMessageElement = styled.div`
    box-shadow: ${props => props.theme.boxShadow};
    display: flex;
    justify-content: space-between;
    background-color: ${props => props.theme[props.messageType]};
    border-radius: ${props => props.theme.borderRadius};
    width: 100%;
    max-width: ${props => props.maxWidth};
    padding: ${props => props.theme.padding}px;
    margin-bottom: ${props => props.theme.padding}px;
    &:last-child,
    &:only-child {
        margin-bottom: 0;
    }
`;


const PopUpMessage = ({fix, children, handleClose, duration, ...others}) => {

    useEffect(() => {
        if(!fix){
            let timer = setTimeout(() => {
                handleClose();
            }, duration);
    
            return () => {clearTimeout(timer)};
        }
    }, []);

    return (
        <PopUpMessageElement mounting unmounting {...others}>
            <PopUpMessageText h3 color="inverse">{children}</PopUpMessageText>
            <IconButton iconClass="fas fa-times" onClick={handleClose}/>
        </PopUpMessageElement>
    )
}

const messageTypes = ["error", "warning", "success", "info"];

PopUpMessage.propTypes = {
    handleClose: PropTypes.func.isRequired,
    messageType: PropTypes.oneOf(messageTypes),
    duration: PropTypes.number,
    fix: PropTypes.bool,
    maxWidth: PropTypes.string
}

PopUpMessage.defaultProps = {
    messageType: "info",
    duration: 6000,
    fix: false,
    maxWidth: '100%'
}

export default PopUpMessage;