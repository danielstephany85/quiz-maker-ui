import styled from 'styled-components';
import PropTypes from 'prop-types';

const positionOptions = ['left', 'right', 'top', 'bottom'];

const setPositions = (props) => {
    let styles = "";
    for (let position of positionOptions){
        if(typeof props[position] !== 'undefined'){
            styles += `${position}: ${props[position]}; `;
        }
    }

    return styles;
}


const PopUpMessageContainer = styled.div`
    display: inline-block;
    padding: ${props => props.theme.padding}px;
    max-width: 100%;
    position: fixed;
    ${setPositions}
    z-index: 10000;
`;

PopUpMessageContainer.proptypes = {
    left: PropTypes.string,
    right: PropTypes.string,
    top: PropTypes.string,
    bottom: PropTypes.string
}

PopUpMessageContainer.defaultProps = {
    bottom: "0",
    left: "0"
}

export default PopUpMessageContainer;