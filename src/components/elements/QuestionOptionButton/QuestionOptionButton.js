import styled from 'styled-components';

const QuestionOptionButton = styled.button`
    background-color: ${props => props.theme.bgSecondary};
    border-color: ${props => props.theme.secondaryDark};
    display: inline-flex;
    align-items: center;
    justify-content: center;
    height: 40px;
    width: 40px;
    padding: 0;
    font-size: 18px;
    color:  ${props => props.theme.secondaryDark};
    text-decoration: none;
    font-weight: 500;
    border-radius: ${props => props.theme.borderRadius};
    box-shadow: none;
    outline: none;
    border-width: 2px;
    border-style: solid;
    transition: 0.25s ease;
    cursor: pointer;
    &:hover {
        border-color: ${props => props.theme.secondary};
        color: ${props => props.theme.secondary};
        box-shadow: ${props => props.theme.boxShadow};
    }
    ${ props => props.active ?
        `background-color: ${props.theme.secondaryDark};
        color: #fff;
        &:hover {
            background-color: ${props.theme.secondaryDark};
            border-color: ${props.theme.secondaryDark};
            color: #fff;
            box-shadow: none;
        }`
        :
        ""
    }
    ${ props => props.disabled ? 
        `border-color: ${props.theme.placeHolderColor};
        color:  ${props.theme.placeHolderColor};
        opacity: 0.8;
        cursor: not-allowed;
        &:hover {
            border-color: ${props.theme.placeHolderColor};
            color:  ${props.theme.placeHolderColor};
            box-shadow: none;
        }`
        :
        ""
    }
`;

export default QuestionOptionButton;