import styled from 'styled-components';

const QuestionFooterBox = styled.div`
    padding: ${props => props.theme.padding}px; 
    padding-top: 0px; 
    z-index: 2;
`;

export default QuestionFooterBox;