import styled from 'styled-components';
import PropTypes from 'prop-types';
import Sheet from '@components/elements/Sheet';

const QuestionFooter = styled(Sheet)`
    border-top: 4px solid ${props => props.theme.primary};
    padding: ${props => props.theme.padding}px; 
`;

export default QuestionFooter;