import React from 'react';
import styled from 'styled-components';

const textLink = ({component, children, ...others}) => {
    const Component = component;
    if (Component) {
        return <Component {...others} >{children}</Component>
    } else {
        return <a {...others} >{children}</a>
    }
}

export const TextLink = styled(textLink)`
    display: ${props => props.display ? props.display : "inline"};
    trasnsition: color 0.2s ease;
    text-decoration: none;
    color: ${props => props.primary ? props.theme.primaryDark : props.theme.secondaryDark};
    cursor: pointer;
    &:hover {
        color: ${props => props.primary ? props.theme.primary : props.theme.secondary};
    }
`;
