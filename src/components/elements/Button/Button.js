import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { LoadingSpan } from './ButtonElements';

const setProperties = (bgColor, borderColor, textColor, hoverColor, props) => {
    return `
        background-color: ${bgColor};
        border-color: ${borderColor};
        color: ${textColor};
        &:hover {
            ${props.outlined ? "" : "background-color: " + hoverColor + ";"}
            ${!props.outlined ? "" : "color: " + hoverColor + ";"}
            border-color: ${hoverColor};
            box-shadow: ${props.theme.boxShadow};
        }
    `;
}

const setColorProperties = props => {
    const { primary, primaryDark, secondary, secondaryDark, textOnPrimary, textOnSecondary, textDefault, bgSecondary, defaultButtonColor } = props.theme;

    if (props.primary && props.outlined){
        return setProperties(bgSecondary, primaryDark, primaryDark, primary, props);
    } else if (props.secondary && props.outlined) {
        return setProperties(bgSecondary, secondaryDark, secondaryDark, secondary, props);
    } else if (props.primary){
        return setProperties(primary, primary, textOnPrimary, primaryDark, props);
    } else if (props.secondary) {
        return setProperties(secondary, secondary, textOnSecondary, secondaryDark, props);
    } else {
        props.outlined = true;
        return setProperties(bgSecondary, defaultButtonColor, defaultButtonColor, textDefault, props);
    }

}

const button = ({ className, children, component, secondary, primary, outlined, loading, type, ...others}) => {
    let Component = component,
    loadingSpan = null,
    classes = className,
    loadingProps = {primary, secondary, outlined};

    if(loading){
        loadingSpan = <LoadingSpan {...loadingProps}><span></span></LoadingSpan>;
        classes = className + ' loading';
    }

    if (Component){
    return <Component className={classes} {...others}>{children}{loadingSpan}</Component>
    } else {
        return <button type={type} className={classes} {...others}>{children}{loadingSpan}</button>
    }
}


export const Button = styled(button)`
    ${props => setColorProperties(props)};
    display: inline-block;
    text-decoration: none;
    font-weight: 500;
    border-radius: ${props => props.theme.borderRadius};
    padding: 8px 16px;
    box-shadow: none;
    outline: none;
    border-width: 2px;
    border-style: solid;
    transition: 0.25s ease;
    cursor: pointer;
    margin-right: ${props => props.theme.padding}px;
    position: relative;
    transition: color 0.25s ease;
    &:only-child,
    &:last-child {
        margin-right: 0;
    }
    &.loading {
        color: transparent;
    }
`;

Button.propTypes = {
    primary: PropTypes.bool,
    secondary: PropTypes.bool,
    outlined: PropTypes.bool,
    type: PropTypes.string
}

Button.defaultProps = {
    primary: false,
    secondary: false,
    outlined: false,
    type: 'button'
}