import styled, { keyframes } from 'styled-components';

const setLoadingColor = (props) => {
    if (props.primary && props.outlined) {
        return props.theme.primary;
    } else if (props.secondary && props.outlined) {
        return props.theme.secondary;
    } else if (props.primary || props.secondary) {
        return props.theme.bgSecondary
    } else {
        return props.theme.textDefault;
    }
}

const buttonLoad = keyframes`
    0% {
        opacity: 0;
        transform: translateX(calc(-100% + 4px));
    }
    10% {
        opacity: 1;
        transform: translateX(calc(-100% + 4px));
    }
    90% {
        opacity: 1;
        transform: translateX(calc(100% - 4px));
    }
    100% {
        opacity: 0;
        transform: translateX(calc(100% - 4px));
    }
`;

export const LoadingSpan = styled.span`
    border-radius: 4px;
    display: flex;
    height: 4px;
    width: calc(100% - ${props => props.theme.padding * 2}px);
    position: absolute;
    top: calc(50% - 2px);
    left: ${props => props.theme.padding}px; 
    overflow: hidden;
    & > span {
        display: block;
        width: 100%;
        right: 100%;
        border-radius: 4px;
        background-color: ${props => setLoadingColor(props)};
        animation: ${buttonLoad} 2s ease 0s infinite forwards;
        transform: translateX(calc(-100% + 4px))
        opacity: 0;
    }
`;