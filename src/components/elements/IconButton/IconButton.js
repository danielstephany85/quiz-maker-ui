import React from 'react';
import styled from 'styled-components';

const iconButton = ({className, iconClass, ...others}) => {

    return (
        <button className={className} {...others}>
            <i className={iconClass}></i>
        </button>
    );
}

const IconButton = styled(iconButton)`
    border-radius: ${props => props.theme.borderRadius};
    background-color: transparent;
    border: none;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    height: 32px;
    width: 32px;
    font-size: 20px;
    color: ${props => props.theme.textInverse};
    &:active,
    &:focus {
        box-shadow: none;
        outline: none;
        border: 2px solid ${props => props.theme.textInverse};
    }
`;

export default IconButton;