import styled from 'styled-components';

const GridContainer = styled.div`
    margin-left: -${props => props.theme.hPadding}px;
    margin-right: -${props => props.theme.hPadding}px;
`;

export default GridContainer;