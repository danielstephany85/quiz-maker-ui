import styled from 'styled-components';

const MainContent = styled.main`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    height: 100vh;
    margin: 0 auto;
    padding: 0;
    overflow: auto;
    position: relative;
`;

export default MainContent;