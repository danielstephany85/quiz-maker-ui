import styled from 'styled-components';
import PropTypes from 'prop-types';

const sizeOptions = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];

const setWidth = (props, sizeVarient) => {
    let size = props[sizeVarient];
    if(size){
        let parsedSize = parseInt(size);
        let width = parsedSize * 8.333
    
        return `width: ${width.toFixed(2) }%;`;
    }
    return null;
}

export const GridItem = styled.div`
    display: inline-block;
    ${props => props.grow ? "flex-grow: 1;" : null}
    ${props => setWidth(props, 'xs')}
    text-align: ${props => props.align};
    padding-left: ${props => props.paddingOff ? 0 : props.theme.hPadding}px;
    padding-right: ${props => props.paddingOff ? 0 : props.theme.hPadding}px;
    margin-top: ${props => props.marginTop};
    margin-bottom: ${props => props.marginBottom};
    @media (min-width: 500px) {
        ${props => setWidth(props, 'sm')}
    }
    @media (min-width: 768px) {
        ${props => setWidth(props, 'md')}
    }
    @media (min-width: 980px) {
        ${props => setWidth(props, 'lg')}
    }
    @media (min-width: 1200px) {
        ${props => setWidth(props, 'xl')}
    }
`;

GridItem.propTypes = {
    align: PropTypes.oneOf(["left","right", "center"]),
    paddingOff: PropTypes.bool,
    marginTop: PropTypes.string,
    marginBottom: PropTypes.string,
    xs: PropTypes.oneOf(sizeOptions),
    sm: PropTypes.oneOf(sizeOptions),
    md: PropTypes.oneOf(sizeOptions),
    lg: PropTypes.oneOf(sizeOptions),
    xg: PropTypes.oneOf(sizeOptions),
    grow: PropTypes.bool
}

GridItem.defaultProps = {
    align: "left",
    marginTop: "0px",
    marginbottom: "0px",
}