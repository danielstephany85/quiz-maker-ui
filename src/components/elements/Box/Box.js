import styled from 'styled-components';
import PropTypes from 'prop-types';

const Box = styled.div`
    ${props => props.grow ? 'flex-grow: 1;' : ''}
    width: 100%;
    max-width: ${props => props.maxWidth};
    padding: ${props => props.theme.vPadding}px ${props => props.theme.hPadding}px;
    ${props => props.paddingTop ? `padding-top: ${props.paddingTop};` : ''}
    ${props => props.paddingBottom ? `padding-Bottom: ${props.paddingBottom};` : ''}
    margin-left: auto;
    margin-right: auto;
    margin-top: ${props => props.marginTop};
    margin-bottom: ${props => props.marginBottom};
    text-align: ${props => props.align};
    overflow: ${props => props.overflow};
`;

Box.propTypes = {
    marginTop: PropTypes.string,
    marginBottom: PropTypes.string,
    paddingTop: PropTypes.string,
    paddingBottom: PropTypes.string,
    maxWidth: PropTypes.string,
    overflow: PropTypes.oneOf(['visible', 'hidden', 'scroll', 'auto']),
    align: PropTypes.oneOf(["left", "right", "center"]),
    grow: PropTypes.bool
}

Box.defaultProps = {
    marginTop: "0",
    marginBottom: "0",
    maxWidth: "100%",
    overflow: 'visible',
    align: "left",
    paddingTop: null,
    paddingBottom: null
}

export default Box;