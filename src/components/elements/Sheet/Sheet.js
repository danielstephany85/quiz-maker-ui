import styled from 'styled-components';
import PropTypes from 'prop-types';
import Box from '@components/elements/Box';

const Sheet = styled(Box)`
    border-radius: ${props => props.theme.borderRadius};
    box-shadow: ${props => props.theme.boxShadow};
    background-color: ${props => props.theme.bgSecondary};
    ${props => props.vPadding ? `padding-top: ${props.vPadding}; padding-bottom: ${props.vPadding};` : ''}
    ${props => props.hPadding ? `padding-top: ${props.hPadding}; padding-bottom: ${props.hPadding};` : ''}
`;

Box.Sheet = {
    marginTop: PropTypes.string,
    marginBottom: PropTypes.string,
    maxWidth: PropTypes.string,
    align: PropTypes.oneOf(["left", "right", "center"]),
    vPadding: PropTypes.string,
    hPadding: PropTypes.string,
}

Box.Sheet = {
    marginTop: "0",
    marginBottom: "0",
    maxWidth: "100%",
    align: "left"
}

export default Sheet;