import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types'; 

const modalHeader = ({className, children, action}) => {

    return (
        <div className={className}>
            <h3 className="modal-header__text">{children}</h3>
            <div className="modal-header__actions">
                {action ? <button className="modal-header__button" onClick={action}>close</button> : null}
            </div>
        </div>
    );
}

const ModalHeader = styled(modalHeader)`
    display: flex;
    justify-content: space-between;
    background-color: ${props => props.theme.primary};
    padding: ${props => props.theme.padding}px;
    .modal-header__text {
        margin: 0;
        flex-grow: 1;
        color: ${props => props.theme.textInverse};
    }
    .modal-header__button {
            background-color: transparent;
            border: none;
            font-weight: 500;
            font-size: 14px;
            color: ${props => props.theme.textInverse};
            padding: ${props => props.theme.padding / 2}px ${props => props.theme.padding}px;
            cursor: pointer;
            &:focus {
                outline: none;
                text-decoration: underline;
                box-shadow: none;
            }
        }
`;


ModalHeader.propTypes = {
    action: PropTypes.func
}

export default ModalHeader;