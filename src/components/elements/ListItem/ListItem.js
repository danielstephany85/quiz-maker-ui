import styled from 'styled-components';

export const ListItem = styled.li`
    display: block;
    padding: 0;
    margin: 0;
`;