import styled from 'styled-components';

const TextButton = styled.button`
    background-color: transparent;
    border: none;
    outline: none;
    font-size: ${props => props.theme.fontSize}px;
    color: ${props => props.inverse ? props.theme.textInverse : props.theme.textDefault};
    cursor: pointer;
    &:focus {
        box-shadow: none;
        text-decoration: underline;
    }
`;

export default TextButton