import React from 'react';
import {Route} from 'react-router-dom';
import MainErrorBoundery from '@components/MainErrorBoundry';

export default function SafeRoute({render, component, children, ...others}){
    const renderObject = {}
    if(render){
        renderObject.render = props => <MainErrorBoundery {...props}>{render(props)}</MainErrorBoundery>;
    } else if (component) {
        renderObject.component = props => <MainErrorBoundery {...props}>{component(props)}</MainErrorBoundery>;
    } else if (children) {
        renderObject.children = props => <MainErrorBoundery {...props}>{children(props)}</MainErrorBoundery>;
    }

    return <Route {...renderObject} {...others}/>
}