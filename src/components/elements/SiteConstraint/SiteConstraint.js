import styled from 'styled-components';

const SiteConstraint = styled.div`
    display: flex;
    background-color: ${props => props.theme.bgPrimary};
    min-height: 100vh;
    position: relative;
    overflow: hidden;
`

export default SiteConstraint;