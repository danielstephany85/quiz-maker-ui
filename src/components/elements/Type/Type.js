import React from 'react';
import PropTypes from 'prop-types';
import {Span, P, H1, H2, H3} from './TypeElements';

const Type = ({span, p, h1, h2, h3, children, ...others}) => {

    if(span) {
        return <Span {...others}>{children}</Span> 
    } else if(p) {
        return <P {...others}>{children}</P> 
    } else if(h1) {
        return <H1 {...others}>{children}</H1> 
    } else if(h2) {
        return <H2 {...others}>{children}</H2> 
    } else if (h3) {
        return <H3 {...others}>{children}</H3>
    }
}

Type.propTypes = {
    color: PropTypes.oneOf(["primary", "secondary", "onPrimary", "onSecondary", "placeholder", "inverse"]),
    marginTopOff: PropTypes.bool,
    marginBottomOff: PropTypes.bool
}

export default Type;