import styled from 'styled-components';

const setColor = props => {
    const { primary, secondary, textOnPrimary, textOnSecondary, textDefault, textInverse, placeHolderColor} = props.theme;
    const color = props.color ? props.color.toLowerCase() : "";

    switch(color){
        case "primary":
            return primary;
        case "secondary":
            return secondary;
        case "onprimary":
            return textOnPrimary;
        case "onsecondary":
            return textOnSecondary;
        case "inverse":
            return textInverse;
        case "placeholder":
            return placeHolderColor;
        default:
            return textDefault;
    }
}

const base = (props, sizeInPix) => `
    color: ${setColor(props)};
    font-size: ${sizeInPix / props.theme.baseFontSize}rem;
    margin-left: 0;
    margin-right: 0;
    margin-top: ${props.marginTopOff ? 0 : (sizeInPix / 1.5) / props.theme.baseFontSize }rem;
    margin-bottom: ${props.marginBottomOff ? 0 : (sizeInPix / 1.5) / props.theme.baseFontSize }rem;
`;

export const Span = styled.span`
    color: ${props => setColor(props)};
`;

export const P = styled.p`
    ${props => base(props, 18)}
`;

export const A = styled.a`
    ${props => base(props, 16)}
`;

export const H1 = styled.h1`
    ${props => base(props, 32)}
`;

export const H2 = styled.h3`
    ${props => base(props, 26)}
`;

export const H3 = styled.h3`
    ${props => base(props, 20)}
`;

