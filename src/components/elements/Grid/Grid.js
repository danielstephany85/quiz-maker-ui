import styled from 'styled-components';
import PropTypes from 'prop-types';

export const Grid = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 0 ${props => props.paddingOff ?  0 : props.theme.hPadding}px;
    width: 100%;
`;

Grid.propTypes = {
    paddingOff: PropTypes.bool
}

