const user = {
    "id": "0809809",
    "firstName": "test name",
    "lastName": "test last Name",
    "email": "email@email.com",
    "pending tests": [],
}

const quizData = {
    "Quiz": {
        "Questions": [
            {
                "order": 0,
                "id": "questionId",
                "type": "multipleChoice",
                "questionText": "example question text",
                "answerOptions": {
                    "0": "option 1",
                    "1": "option 2",
                    "2": "option 3",
                    "3": "option 4",
                        },
                "answerID": "id of answer",
                // example answer data 
                "answer": "2"
            },
            {
                "order": 1,
                "id": "questionid",
                "type": "boolean",
                "questionText": "example question text",
                "answerOptions": {
                    "0": true,
                    "1": false
                },
                "answerID": "id of answer",
                // example answer data 
                "answer": "2"
            },
            {
                "order": 2,
                "id": "questionid",
                "type": "match",
                "questionText": "match the description with the term",
                "questonOptions": {
                    "0": "term 1",
                    "1": "term 2",
                    "2": "term 3",
                    "3": "term 4"
                },
                "answerOptions": {
                    "0": "option 1",
                    "1": "option 2",
                    "2": "option 3",
                    "3": "option 4"
                },
                "answerID": "id of answer",
                // example answer data 
                "answer": [3201],
                // or 
                "answer": "3,2,0,1"
            },
            {
                "order": 3,
                "id": "questionid",
                "type": "text",
                "questionText": "example question text",
                "answerEntry": "example text answer",
                "answerID": "id of answer"
            }
        ]
    }
}

// the answer key should be the question id

const answerSet = {
    "userId": 92302940923,
    "answers": {
        "3234342": {
            "answer": "2",
            "questionsID": "23234234"
        },
        "3234342": {
            "answer": "2",
            "questionsID": "23234234"
        }
    }
}